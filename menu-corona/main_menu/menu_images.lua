--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:c3e726c6b28ff7d704dedd210271cdde:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- background
            x=2,
            y=2,
            width=803,
            height=504,

        },
        {
            -- btn1
            x=807,
            y=2,
            width=161,
            height=45,

        },
        {
            -- btn2
            x=807,
            y=143,
            width=159,
            height=45,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 161,
            sourceHeight = 45
        },
        {
            -- btn3
            x=807,
            y=96,
            width=159,
            height=45,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 161,
            sourceHeight = 45
        },
        {
            -- btn4
            x=807,
            y=49,
            width=159,
            height=45,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 161,
            sourceHeight = 45
        },
    },
    
    sheetContentWidth = 1024,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["background"] = 1,
    ["startGame"] = 2,
    ["startMiniGame"] = 3,
    ["highScores"] = 4,
    ["exit"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
