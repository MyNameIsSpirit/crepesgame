--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:c2190680e74bf1330bde1357827059aa:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- btn1
            x=1655,
            y=281,
            width=330,
            height=91,

        },
        {
            -- btn2
            x=1655,
            y=188,
            width=330,
            height=91,

        },
        {
            -- btn3
            x=1655,
            y=95,
            width=330,
            height=91,

        },
        {
            -- btn4
            x=1655,
            y=2,
            width=330,
            height=91,

        },
        {
            -- start
            x=2,
            y=2,
            width=1651,
            height=948,

        },
    },
    
    sheetContentWidth = 2048,
    sheetContentHeight = 1024
}

SheetInfo.frameIndex =
{

    ["startGame"] = 1,
    ["startMiniGame"] = 2,
    ["highScores"] = 3,
    ["exit"] = 4,
    ["background"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
