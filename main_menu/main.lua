
--local settings = require("libs.settings").getInstance()
local storyboard = require( "storyboard" )
storyboard.purgeOnSceneChange = true
local scene = storyboard.newScene()

local widget = require( "widget" )
local sheetInfo = require("main_menu.menu_images")
local sheetImage = graphics.newImageSheet("main_menu/menu_images.png",sheetInfo:getSheet())
local startGameButton
local miniGameButton
local highscoresButton
local exitButton



-- Called when the scene's view does not exist:
function scene:createScene( event )
  
	local group = self.view
  
  -- Ubicando background
  
  local background = display.newImage(sheetImage,sheetInfo:getFrameIndex("background"),0,0)
  background:scale(0.83,0.81)
  background.x = 500
  background.y = 385
  group:insert(background)

  -- local centralIcon = display.newImage(sheetImage,sheetInfo:getFrameIndex("crepesIcon"))
  -- centralIcon.x = display.contentWidth / 2
  -- centralIcon.y = display.contentHeight / 2
  -- group:insert(centralIcon)
  
  --Ubicando boton start
  --local startGameButton = display.newImage(sheetImage,sheetInfo:getFrameIndex("startGame"))
  startGameButton = widget.newButton
  {
   sheet = sheetImage,  --reference to the image sheet
   defaultFrame = sheetInfo:getFrameIndex("startGame"),     --number of the "default" frame
   overFrame = sheetInfo:getFrameIndex("startGame")        --number of the "over" frame
  }
  startGameButton.x = display.contentWidth / 2
  startGameButton.y = 420
  startGameButton:scale(1,1)
  group:insert(startGameButton)
  
  
  --Ubicando boton minigame
  miniGameButton = widget.newButton
  {
   sheet = sheetImage,  --reference to the image sheet
   defaultFrame = sheetInfo:getFrameIndex("startMiniGame"),     --number of the "default" frame
   overFrame = sheetInfo:getFrameIndex("startMiniGame")        --number of the "over" frame
  }
  miniGameButton.x = display.contentWidth / 2
  miniGameButton.y = 520
    miniGameButton:scale(1,1)
  group:insert(miniGameButton)
    
  --Ubicando boton highscores
  highscoresButton = widget.newButton
  {
   sheet = sheetImage,  --reference to the image sheet
   defaultFrame = sheetInfo:getFrameIndex("highScores"),     --number of the "default" frame
   overFrame = sheetInfo:getFrameIndex("highScores")        --number of the "over" frame
  }
  highscoresButton.x = display.contentWidth / 2
  highscoresButton.y = 620
  highscoresButton:scale(1,1)
  group:insert(highscoresButton)
    
  --Ubicando boton exit
  exitButton = widget.newButton
  {
   sheet = sheetImage,  --reference to the image sheet
   defaultFrame = sheetInfo:getFrameIndex("exit"),     --number of the "default" frame
   overFrame = sheetInfo:getFrameIndex("exit")        --number of the "over" frame
  }
   exitButton:scale(1,1)
  exitButton.x = display.contentWidth / 2
  exitButton.y = 720
  group:insert(exitButton)
  
  
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
  
	--Start Game button listener
	function startGameButton:tap(event)
    --storyboard.removeScene( "scenetemplate" )
    storyboard.removeScene("scenetemplate")
    local settings = require("libs.settings").getInstance()
    settings:set("level",1)
    local mainScores = HighScores:new("main")
    mainScores:setActualScore("main",0)
	  
    local function moveToLevel()
      storyboard.gotoScene("scenetemplate")
    end
    timer.performWithDelay(50,moveToLevel)
	end
	print("adding listeners")
	startGameButton:addEventListener("tap", startGameButton)
	
	--Start Mini Game button listener
	function miniGameButton:tap(event)
    storyboard.removeScene("cenes.minigame.minigame")
    local function moveToLevel()
      storyboard.gotoScene( "scenes.minigame.minigame" )
    end
    timer.performWithDelay(50,moveToLevel)
	  
	end
	print("adding listeners")
	miniGameButton:addEventListener("tap", miniGameButton)
	
	--High Scores button listener
	function highscoresButton:tap(event)
    local options =
    {
        effect = "fade",
        time = 400,
        isModal = true
    }

    storyboard.showOverlay( "scenes.scores", options )
	end
	print("adding listeners")
	highscoresButton:addEventListener("tap", highscoresButton)
	
	--Exit button listener
	function exitButton:tap(event)
	  function quit() os.exit() end 
	  timer.performWithDelay(100,quit)
	end
	print("adding listeners")
	exitButton:addEventListener("tap", exitButton)
	
	--
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
	--storyboard.removeScene("scenetemplate")
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end


---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene