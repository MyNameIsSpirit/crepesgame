--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:f610e6d0b9202bb80a19d5337dcdc9ac:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- ingredients_save_002 01
            x=109,
            y=174,
            width=45,
            height=37,

            sourceX = 0,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_01 02
            x=199,
            y=101,
            width=43,
            height=33,

            sourceX = 2,
            sourceY = 3,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_03 03
            x=199,
            y=37,
            width=45,
            height=27,

            sourceX = 0,
            sourceY = 10,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_04 04
            x=35,
            y=143,
            width=43,
            height=43,

            sourceX = 0,
            sourceY = 1,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_05 05
            x=45,
            y=92,
            width=45,
            height=39,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_06 06
            x=80,
            y=133,
            width=39,
            height=39,

            sourceX = 1,
            sourceY = 1,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_07 07
            x=131,
            y=43,
            width=45,
            height=35,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_08 08
            x=130,
            y=84,
            width=43,
            height=37,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_10 09
            x=121,
            y=125,
            width=43,
            height=37,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_11 10
            x=154,
            y=213,
            width=43,
            height=37,

            sourceX = 0,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_12 11
            x=199,
            y=66,
            width=43,
            height=33,

            sourceX = 1,
            sourceY = 7,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_13 12
            x=135,
            y=2,
            width=43,
            height=35,

            sourceX = 2,
            sourceY = 7,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_14 13
            x=2,
            y=96,
            width=41,
            height=45,

            sourceX = 3,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_15 14
            x=86,
            y=43,
            width=43,
            height=39,

            sourceX = 0,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_16 15
            x=64,
            y=188,
            width=43,
            height=41,

            sourceX = 1,
            sourceY = 3,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_17 16
            x=45,
            y=49,
            width=39,
            height=41,

            sourceX = 3,
            sourceY = 3,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_18 17
            x=94,
            y=2,
            width=39,
            height=39,

            sourceX = 1,
            sourceY = 4,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_19 18
            x=2,
            y=190,
            width=31,
            height=45,

            sourceX = 8,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_20 19
            x=199,
            y=2,
            width=45,
            height=33,

            sourceX = 0,
            sourceY = 5,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_21 20
            x=2,
            y=2,
            width=43,
            height=45,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_22 21
            x=47,
            y=2,
            width=45,
            height=39,

            sourceX = 0,
            sourceY = 1,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_23 22
            x=2,
            y=49,
            width=41,
            height=45,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_24 23
            x=2,
            y=143,
            width=31,
            height=45,

            sourceX = 7,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_25 24
            x=109,
            y=213,
            width=43,
            height=37,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_26 25
            x=35,
            y=188,
            width=27,
            height=43,

            sourceX = 9,
            sourceY = 1,
            sourceWidth = 45,
            sourceHeight = 45
        },
        {
            -- ingredients_save_27 26
            x=92,
            y=84,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },
        {
            -- ingredients_save_28 27
            x=214,
            y=175,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },
        {
            -- ingredients_save_29 28
            x=210,
            y=135,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },
        {
            -- ingredients_save_30 29
            x=170,
            y=175,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },
        {
            -- ingredients_save_31 30
            x=220,
            y=215,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },
        {
            -- ingredients_save_32 31
            x=170,
            y=130,
            width=36,
            height=39,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 40,
            sourceHeight = 39
        },



    },
    
    sheetContentWidth = 256,
    sheetContentHeight = 256
}

SheetInfo.frameIndex =
{
    ["0"] = 2,
    ["1"] = 3,
    ["2"] = 4,
    ["3"] = 5,
    ["4"] = 6,
    ["5"] = 8,
    ["6"] = 9,
    ["7"] = 11,
    ["8"] = 12,
    ["9"] = 14,
    ["10"] = 15,
    ["11"] = 16,
    ["12"] = 17,
    ["13"] = 19,
    ["14"] = 20,
    ["15"] = 21,
    ["16"] = 22,
    ["17"] = 25,
    ["18"] = 26,
    ["19"] = 27,
    ["20"] = 29,
    ["21"] = 30,
    ["22"] = 28,
    ["23"] = 31,
    -- ["24"] = 32,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
