--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:d41aa4aefee1d1bf8308e5c35c18c8b6:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- cream1
            x=131,
            y=2,
            width=127,
            height=175,

        },
        {
            -- cream2
            x=385,
            y=2,
            width=121,
            height=175,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 127,
            sourceHeight = 175
        },
        {
            -- cream3
            x=260,
            y=2,
            width=123,
            height=175,

            sourceX = 0,
            sourceY = 0,
            sourceWidth = 127,
            sourceHeight = 175
        },
        {
            -- cream4
            x=2,
            y=179,
            width=127,
            height=175,

        },
        {
            -- cream5
            x=2,
            y=2,
            width=127,
            height=175,

        },
    },
    
    sheetContentWidth = 512,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["0"] = 1,
    ["1"] = 2,
    ["2"] = 3,
    ["3"] = 4,
    ["4"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
