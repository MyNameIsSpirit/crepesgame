--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:8fd0a1f462dc4c746f4653f2a7ab86af:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- choloate
            x=2,
            y=374,
            width=155,
            height=91,

            sourceX = 10,
            sourceY = 0,
            sourceWidth = 167,
            sourceHeight = 91
        },
        {
            -- cream
            x=2,
            y=281,
            width=155,
            height=91,

            sourceX = 10,
            sourceY = 0,
            sourceWidth = 167,
            sourceHeight = 91
        },
        {
            -- darkchocolate
            x=2,
            y=188,
            width=155,
            height=91,

            sourceX = 10,
            sourceY = 0,
            sourceWidth = 167,
            sourceHeight = 91
        },
        {
            -- vaillla
            x=2,
            y=95,
            width=155,
            height=91,

            sourceX = 10,
            sourceY = 0,
            sourceWidth = 167,
            sourceHeight = 91
        },
        {
            -- whitechocolate
            x=2,
            y=2,
            width=155,
            height=91,

            sourceX = 10,
            sourceY = 0,
            sourceWidth = 167,
            sourceHeight = 91
        },
    },
    
    sheetContentWidth = 256,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["choloate"] = 1,
    ["cream"] = 2,
    ["darkchocolate"] = 3,
    ["vaillla"] = 4,
    ["whitechocolate"] = 5,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
