--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:211ada6d578749bb8dd67071fbb4fbb0:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- toon10147
            x=258,
            y=605,
            width=126,
            height=197,

            sourceX = 155,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10148
            x=2,
            y=800,
            width=126,
            height=197,

            sourceX = 155,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10149
            x=130,
            y=605,
            width=126,
            height=197,

            sourceX = 156,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10150
            x=514,
            y=605,
            width=126,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10151
            x=386,
            y=408,
            width=126,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10152
            x=258,
            y=406,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10153
            x=2,
            y=601,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10154
            x=256,
            y=804,
            width=124,
            height=197,

            sourceX = 161,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10155
            x=2,
            y=999,
            width=124,
            height=197,

            sourceX = 161,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10156
            x=634,
            y=806,
            width=124,
            height=195,

            sourceX = 160,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10157
            x=634,
            y=1003,
            width=122,
            height=195,

            sourceX = 161,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10158
            x=130,
            y=804,
            width=124,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10159
            x=812,
            y=804,
            width=128,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10160
            x=548,
            y=404,
            width=132,
            height=199,

            sourceX = 154,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10161
            x=684,
            y=402,
            width=132,
            height=199,

            sourceX = 153,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10162
            x=818,
            y=207,
            width=132,
            height=201,

            sourceX = 151,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10163
            x=684,
            y=199,
            width=132,
            height=201,

            sourceX = 149,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10164
            x=550,
            y=199,
            width=132,
            height=203,

            sourceX = 147,
            sourceY = 50,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10165
            x=822,
            y=2,
            width=132,
            height=203,

            sourceX = 146,
            sourceY = 50,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10166
            x=278,
            y=2,
            width=134,
            height=203,

            sourceX = 144,
            sourceY = 50,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10167
            x=140,
            y=2,
            width=136,
            height=203,

            sourceX = 142,
            sourceY = 50,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10168
            x=2,
            y=2,
            width=136,
            height=203,

            sourceX = 142,
            sourceY = 50,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10169
            x=414,
            y=2,
            width=134,
            height=201,

            sourceX = 143,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10170
            x=414,
            y=205,
            width=132,
            height=201,

            sourceX = 144,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10171
            x=682,
            y=603,
            width=128,
            height=201,

            sourceX = 146,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10172
            x=2,
            y=1198,
            width=122,
            height=199,

            sourceX = 150,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10173
            x=252,
            y=1200,
            width=118,
            height=199,

            sourceX = 152,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10174
            x=2,
            y=1399,
            width=114,
            height=199,

            sourceX = 155,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10175
            x=500,
            y=1196,
            width=110,
            height=199,

            sourceX = 158,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10176
            x=242,
            y=1401,
            width=108,
            height=199,

            sourceX = 159,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10177
            x=758,
            y=1003,
            width=106,
            height=199,

            sourceX = 160,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10178
            x=2,
            y=1600,
            width=106,
            height=201,

            sourceX = 160,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10179
            x=214,
            y=1805,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10180
            x=226,
            y=1602,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10181
            x=352,
            y=1594,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10182
            x=482,
            y=1397,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10183
            x=612,
            y=1200,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10184
            x=108,
            y=1803,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10185
            x=2,
            y=1803,
            width=104,
            height=201,

            sourceX = 161,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10186
            x=118,
            y=1405,
            width=106,
            height=201,

            sourceX = 160,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10187
            x=372,
            y=1391,
            width=108,
            height=201,

            sourceX = 159,
            sourceY = 52,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10188
            x=126,
            y=1204,
            width=114,
            height=199,

            sourceX = 156,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10189
            x=128,
            y=1003,
            width=122,
            height=199,

            sourceX = 150,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10190
            x=812,
            y=605,
            width=128,
            height=197,

            sourceX = 149,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10191
            x=686,
            y=2,
            width=134,
            height=195,

            sourceX = 147,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10192
            x=550,
            y=2,
            width=134,
            height=195,

            sourceX = 150,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10193
            x=818,
            y=410,
            width=132,
            height=193,

            sourceX = 153,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10194
            x=2,
            y=207,
            width=128,
            height=193,

            sourceX = 156,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10195
            x=508,
            y=1001,
            width=124,
            height=193,

            sourceX = 159,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10196
            x=382,
            y=1001,
            width=124,
            height=193,

            sourceX = 159,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10197
            x=376,
            y=1196,
            width=122,
            height=193,

            sourceX = 161,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10198
            x=252,
            y=1003,
            width=122,
            height=195,

            sourceX = 161,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10199
            x=508,
            y=804,
            width=124,
            height=195,

            sourceX = 160,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10200
            x=382,
            y=804,
            width=124,
            height=195,

            sourceX = 160,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10201
            x=386,
            y=607,
            width=126,
            height=195,

            sourceX = 159,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10202
            x=130,
            y=406,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10203
            x=260,
            y=207,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10204
            x=2,
            y=402,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10205
            x=132,
            y=207,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
    },
    
    sheetContentWidth = 1024,
    sheetContentHeight = 2048
}

SheetInfo.frameIndex =
{

    ["toon10147"] = 1,
    ["toon10148"] = 2,
    ["toon10149"] = 3,
    ["toon10150"] = 4,
    ["toon10151"] = 5,
    ["toon10152"] = 6,
    ["toon10153"] = 7,
    ["toon10154"] = 8,
    ["toon10155"] = 9,
    ["toon10156"] = 10,
    ["toon10157"] = 11,
    ["toon10158"] = 12,
    ["toon10159"] = 13,
    ["toon10160"] = 14,
    ["toon10161"] = 15,
    ["toon10162"] = 16,
    ["toon10163"] = 17,
    ["toon10164"] = 18,
    ["toon10165"] = 19,
    ["toon10166"] = 20,
    ["toon10167"] = 21,
    ["toon10168"] = 22,
    ["toon10169"] = 23,
    ["toon10170"] = 24,
    ["toon10171"] = 25,
    ["toon10172"] = 26,
    ["toon10173"] = 27,
    ["toon10174"] = 28,
    ["toon10175"] = 29,
    ["toon10176"] = 30,
    ["toon10177"] = 31,
    ["toon10178"] = 32,
    ["toon10179"] = 33,
    ["toon10180"] = 34,
    ["toon10181"] = 35,
    ["toon10182"] = 36,
    ["toon10183"] = 37,
    ["toon10184"] = 38,
    ["toon10185"] = 39,
    ["toon10186"] = 40,
    ["toon10187"] = 41,
    ["toon10188"] = 42,
    ["toon10189"] = 43,
    ["toon10190"] = 44,
    ["toon10191"] = 45,
    ["toon10192"] = 46,
    ["toon10193"] = 47,
    ["toon10194"] = 48,
    ["toon10195"] = 49,
    ["toon10196"] = 50,
    ["toon10197"] = 51,
    ["toon10198"] = 52,
    ["toon10199"] = 53,
    ["toon10200"] = 54,
    ["toon10201"] = 55,
    ["toon10202"] = 56,
    ["toon10203"] = 57,
    ["toon10204"] = 58,
    ["toon10205"] = 59,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
