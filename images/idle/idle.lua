--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:f9920fd6b4dd3b558babb57333880e38:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- toon10060
            x=660,
            y=342,
            width=108,
            height=168,

            sourceX = 135,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10061
            x=880,
            y=172,
            width=108,
            height=168,

            sourceX = 135,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10062
            x=656,
            y=512,
            width=106,
            height=168,

            sourceX = 136,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10063
            x=770,
            y=172,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10064
            x=660,
            y=172,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10065
            x=546,
            y=852,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10066
            x=546,
            y=682,
            width=108,
            height=168,

            sourceX = 133,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10067
            x=550,
            y=342,
            width=108,
            height=168,

            sourceX = 133,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10068
            x=550,
            y=172,
            width=108,
            height=168,

            sourceX = 133,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10069
            x=878,
            y=342,
            width=106,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10070
            x=770,
            y=342,
            width=106,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10071
            x=546,
            y=512,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10072
            x=878,
            y=2,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10073
            x=768,
            y=2,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10074
            x=658,
            y=2,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10075
            x=436,
            y=852,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10076
            x=436,
            y=682,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10077
            x=440,
            y=342,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10078
            x=548,
            y=2,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10079
            x=440,
            y=172,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10080
            x=436,
            y=512,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10081
            x=330,
            y=342,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10082
            x=438,
            y=2,
            width=108,
            height=168,

            sourceX = 132,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10083
            x=330,
            y=172,
            width=108,
            height=168,

            sourceX = 133,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10084
            x=2,
            y=346,
            width=108,
            height=170,

            sourceX = 133,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10085
            x=2,
            y=174,
            width=108,
            height=170,

            sourceX = 133,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10086
            x=220,
            y=2,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10087
            x=218,
            y=690,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10088
            x=218,
            y=518,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10089
            x=112,
            y=346,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10090
            x=112,
            y=174,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10091
            x=112,
            y=2,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10092
            x=110,
            y=690,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10093
            x=110,
            y=518,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10094
            x=2,
            y=690,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10095
            x=2,
            y=518,
            width=106,
            height=170,

            sourceX = 135,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10096
            x=2,
            y=2,
            width=108,
            height=170,

            sourceX = 134,
            sourceY = 46,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10097
            x=326,
            y=854,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10098
            x=326,
            y=684,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10099
            x=326,
            y=514,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10100
            x=220,
            y=344,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10101
            x=328,
            y=2,
            width=108,
            height=168,

            sourceX = 134,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
        {
            -- toon10102
            x=220,
            y=174,
            width=108,
            height=168,

            sourceX = 133,
            sourceY = 48,
            sourceWidth = 384,
            sourceHeight = 216
        },
    },
    
    sheetContentWidth = 1024,
    sheetContentHeight = 1024
}

SheetInfo.frameIndex =
{

    ["0"] = 1,
    ["1"] = 2,
    ["2"] = 3,
    ["3"] = 4,
    ["7"] = 5,
    ["5"] = 6,
    ["6"] = 7,
    ["7"] = 8,
    ["8"] = 9,
    ["9"] = 10,
    ["10"] = 11,
    ["11"] = 12,
    ["12"] = 13,
    ["13"] = 14,
    ["14"] = 15,
    ["15"] = 16,
    ["16"] = 17,
    ["17"] = 18,
    ["18"] = 19,
    ["19"] = 20,
    ["20"] = 21,
    ["21"] = 22,
    ["22"] = 23,
    ["23"] = 24,
    ["24"] = 25,
    ["25"] = 26,
    ["26"] = 27,
    ["27"] = 28,
    ["28"] = 29,
    ["29"] = 30,
    ["30"] = 31,
    ["31"] = 32,
    ["32"] = 33,
    ["33"] = 34,
    ["34"] = 35,
    ["35"] = 36,
    ["36"] = 37,
    ["37"] = 38,
    ["38"] = 39,
    ["39"] = 40,
    ["40"] = 41,
    ["41"] = 42,
    ["42"] = 43,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
