--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:3b981e6dd6091c64c14677abe39261b1:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- toon10001
            x=520,
            y=601,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10002
            x=128,
            y=999,
            width=118,
            height=197,

            sourceX = 154,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10003
            x=138,
            y=402,
            width=132,
            height=197,

            sourceX = 146,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10004
            x=600,
            y=2,
            width=140,
            height=199,

            sourceX = 144,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10005
            x=286,
            y=201,
            width=134,
            height=199,

            sourceX = 144,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10006
            x=422,
            y=400,
            width=132,
            height=199,

            sourceX = 144,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10007
            x=2,
            y=396,
            width=134,
            height=199,

            sourceX = 142,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10008
            x=150,
            y=201,
            width=134,
            height=199,

            sourceX = 144,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10009
            x=880,
            y=201,
            width=136,
            height=199,

            sourceX = 145,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10010
            x=742,
            y=201,
            width=136,
            height=199,

            sourceX = 148,
            sourceY = 54,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10011
            x=558,
            y=203,
            width=134,
            height=197,

            sourceX = 151,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10012
            x=686,
            y=402,
            width=128,
            height=197,

            sourceX = 154,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10013
            x=248,
            y=999,
            width=118,
            height=195,

            sourceX = 156,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10014
            x=332,
            y=1795,
            width=112,
            height=195,

            sourceX = 155,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10015
            x=436,
            y=1596,
            width=108,
            height=195,

            sourceX = 155,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10016
            x=538,
            y=1395,
            width=104,
            height=195,

            sourceX = 157,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10017
            x=550,
            y=1789,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10018
            x=654,
            y=1198,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10019
            x=434,
            y=1397,
            width=102,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10020
            x=2,
            y=1588,
            width=112,
            height=197,

            sourceX = 150,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10021
            x=382,
            y=997,
            width=118,
            height=197,

            sourceX = 145,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10022
            x=856,
            y=999,
            width=114,
            height=197,

            sourceX = 149,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10023
            x=328,
            y=1397,
            width=104,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10024
            x=226,
            y=1198,
            width=104,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10025
            x=226,
            y=1787,
            width=104,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10026
            x=116,
            y=1389,
            width=108,
            height=197,

            sourceX = 154,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10027
            x=2,
            y=1389,
            width=112,
            height=197,

            sourceX = 150,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10028
            x=620,
            y=997,
            width=116,
            height=197,

            sourceX = 147,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10029
            x=880,
            y=800,
            width=118,
            height=197,

            sourceX = 145,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10030
            x=502,
            y=997,
            width=116,
            height=197,

            sourceX = 147,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10031
            x=114,
            y=1787,
            width=110,
            height=197,

            sourceX = 152,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10032
            x=222,
            y=1588,
            width=104,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10033
            x=332,
            y=1596,
            width=102,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10034
            x=644,
            y=1395,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10035
            x=436,
            y=1198,
            width=112,
            height=195,

            sourceX = 150,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10036
            x=2,
            y=1192,
            width=118,
            height=195,

            sourceX = 145,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10037
            x=738,
            y=999,
            width=116,
            height=195,

            sourceX = 147,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10038
            x=650,
            y=1592,
            width=106,
            height=193,

            sourceX = 155,
            sourceY = 60,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10039
            x=546,
            y=1592,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10040
            x=446,
            y=1793,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10041
            x=550,
            y=1198,
            width=102,
            height=195,

            sourceX = 158,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10042
            x=332,
            y=1198,
            width=102,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10043
            x=116,
            y=1588,
            width=104,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10044
            x=2,
            y=1787,
            width=110,
            height=197,

            sourceX = 153,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10045
            x=760,
            y=800,
            width=118,
            height=197,

            sourceX = 147,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10046
            x=556,
            y=402,
            width=128,
            height=197,

            sourceX = 140,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10047
            x=884,
            y=2,
            width=138,
            height=197,

            sourceX = 134,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10048
            x=742,
            y=2,
            width=140,
            height=197,

            sourceX = 135,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10049
            x=456,
            y=2,
            width=142,
            height=197,

            sourceX = 135,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10050
            x=900,
            y=601,
            width=122,
            height=197,

            sourceX = 155,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10051
            x=634,
            y=800,
            width=124,
            height=195,

            sourceX = 153,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10052
            x=2,
            y=597,
            width=132,
            height=195,

            sourceX = 148,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10053
            x=2,
            y=199,
            width=146,
            height=195,

            sourceX = 143,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10054
            x=2,
            y=2,
            width=154,
            height=195,

            sourceX = 149,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10055
            x=158,
            y=2,
            width=152,
            height=197,

            sourceX = 154,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10056
            x=312,
            y=2,
            width=142,
            height=197,

            sourceX = 156,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10057
            x=422,
            y=201,
            width=134,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10058
            x=272,
            y=402,
            width=128,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10059
            x=392,
            y=601,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10060
            x=264,
            y=601,
            width=126,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10061
            x=256,
            y=800,
            width=124,
            height=197,

            sourceX = 160,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10062
            x=2,
            y=794,
            width=126,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10063
            x=2,
            y=993,
            width=124,
            height=197,

            sourceX = 159,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10064
            x=136,
            y=601,
            width=126,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10065
            x=816,
            y=402,
            width=126,
            height=197,

            sourceX = 157,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10066
            x=130,
            y=800,
            width=124,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10067
            x=774,
            y=601,
            width=124,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10068
            x=648,
            y=601,
            width=124,
            height=197,

            sourceX = 158,
            sourceY = 56,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10069
            x=508,
            y=800,
            width=124,
            height=195,

            sourceX = 157,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
        {
            -- toon10070
            x=382,
            y=800,
            width=124,
            height=195,

            sourceX = 157,
            sourceY = 58,
            sourceWidth = 450,
            sourceHeight = 253
        },
    },
    
    sheetContentWidth = 1024,
    sheetContentHeight = 2048
}

SheetInfo.frameIndex =
{

    ["toon10001"] = 1,
    ["toon10002"] = 2,
    ["toon10003"] = 3,
    ["toon10004"] = 4,
    ["toon10005"] = 5,
    ["toon10006"] = 6,
    ["toon10007"] = 7,
    ["toon10008"] = 8,
    ["toon10009"] = 9,
    ["toon10010"] = 10,
    ["toon10011"] = 11,
    ["toon10012"] = 12,
    ["toon10013"] = 13,
    ["toon10014"] = 14,
    ["toon10015"] = 15,
    ["toon10016"] = 16,
    ["toon10017"] = 17,
    ["toon10018"] = 18,
    ["toon10019"] = 19,
    ["toon10020"] = 20,
    ["toon10021"] = 21,
    ["toon10022"] = 22,
    ["toon10023"] = 23,
    ["toon10024"] = 24,
    ["toon10025"] = 25,
    ["toon10026"] = 26,
    ["toon10027"] = 27,
    ["toon10028"] = 28,
    ["toon10029"] = 29,
    ["toon10030"] = 30,
    ["toon10031"] = 31,
    ["toon10032"] = 32,
    ["toon10033"] = 33,
    ["toon10034"] = 34,
    ["toon10035"] = 35,
    ["toon10036"] = 36,
    ["toon10037"] = 37,
    ["toon10038"] = 38,
    ["toon10039"] = 39,
    ["toon10040"] = 40,
    ["toon10041"] = 41,
    ["toon10042"] = 42,
    ["toon10043"] = 43,
    ["toon10044"] = 44,
    ["toon10045"] = 45,
    ["toon10046"] = 46,
    ["toon10047"] = 47,
    ["toon10048"] = 48,
    ["toon10049"] = 49,
    ["toon10050"] = 50,
    ["toon10051"] = 51,
    ["toon10052"] = 52,
    ["toon10053"] = 53,
    ["toon10054"] = 54,
    ["toon10055"] = 55,
    ["toon10056"] = 56,
    ["toon10057"] = 57,
    ["toon10058"] = 58,
    ["toon10059"] = 59,
    ["toon10060"] = 60,
    ["toon10061"] = 61,
    ["toon10062"] = 62,
    ["toon10063"] = 63,
    ["toon10064"] = 64,
    ["toon10065"] = 65,
    ["toon10066"] = 66,
    ["toon10067"] = 67,
    ["toon10068"] = 68,
    ["toon10069"] = 69,
    ["toon10070"] = 70,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
