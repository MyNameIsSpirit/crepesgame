-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
--require("mobdebug").start()
local storyboard = require "storyboard"
storyboard.purgeOnSceneChange = true
-- load scenetemplate.lua

storyboard.gotoScene( "main_menu.main" )

-- Add any objects that should appear on all scenes below (e.g. tab bar, hud, etc.):