----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
storyboard.purgeOnSceneChange = false
local scene = storyboard.newScene()

local ui = require("ui")
local movieclip = require "movieclip"
local Crepe = require "libs.crepe"
local Ingredients = require "libs.ingredients"
local Character = require "libs.character"
local json = require("json")
local Files = require "libs.files"

  local kindlePadding = 0;

  print(system.getInfo("model"))
  if ( system.getInfo("model") == "Kindle Fire" or system.getInfo("model") == "KFJWI" or system.getInfo("model") == "KFTT" or system.getInfo("model") == "BNRV200" ) then
   kindlePadding = 70
  end
----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	
end

function scene:finishLevel(status)
  local group = scene.view
  local HighScores = require "libs.scores"
  local mainScores = HighScores:new("main")



  if status == "win" then
    print("level won!")
    self.gameIsPaused = true
    local numLevel = self.numlevel + 1
    if numLevel > 10 then
      numLevel = 1
    end
    local settings = require("libs.settings").getInstance()
    
    
    settings:set("level",numLevel)
    local bonus=(self.secondsNumber + self.minutesNumber*60)*10
    local score = mainScores:getActualScore("main") + bonus
    mainScores:setActualScore("main",score)
    
    local options =
    {
        effect = "fade",
        time = 400,
        params = {
          bonus = bonus,
          score = score,
          level = numLevel
        }
    }
    storyboard.showOverlay("scenes.levelPassed", options)
    
  else
    self.gameIsPaused = true
    
    local options =
    {
        effect = "fade",
        time = 400
    }
    storyboard.showOverlay("scenes.gameOver", options)
  end
end

function scene:pauseGame()
  
  self.gameIsPaused = true
  local options =
    {
        effect = "fade",
        time = 400,
        isModal = true,
        params = {parentScene = scene}
        
    }
    storyboard.showOverlay( "scenes.gamePaused", options )
    
end

function scene:enterScene( event )
  local group = self.view
  self.gameIsPaused = false
	--local gameMusic1 = audio.loadStream("audio/backgroundMusic.mp3")
  local gameMusic1 = audio.loadStream("scenes/minigame/audio/minigame.mp3")
	local backgroundMusicChannel = audio.play(gameMusic1, {channel = 1, loops = -1, fadein = 5000 } )
	audio.setVolume( 0.5 ) 
	local kitchenSound = audio.loadStream("audio/kitchen.mp3")

	local sheetInfoCreamNames = require("images.creams.creamnames")
	local sheetImageCreamNames = graphics.newImageSheet("images/creams/creamnames.png",sheetInfoCreamNames:getSheet())
  
	--Configs variables
	local currentLevel = 1
	crepes = {}
	spaces = 6 --0 to 6
	local isFreeze = false
	local newTrayCounter = 0

	--Sheets
	local sheetInfoCharacter = require("images.idle.idle")
	local sheetImageCharacter = graphics.newImageSheet("images/idle/idle.png",sheetInfoCharacter:getSheet())
	local sheetInfoCharacterHappy = require("images.idle.happy")
	local sheetImageCharacterHappy = graphics.newImageSheet("images/idle/happy.png",sheetInfoCharacterHappy:getSheet())
	local sheetInfoCharacterSad = require("images.idle.sad")
	local sheetImageCharacterSad = graphics.newImageSheet("images/idle/sad.png",sheetInfoCharacterSad:getSheet())

	--Background stage
	--[[numlevel = 1
	if Files:loadTableDB("level.json") ~= nil then
		numlevel = Files:loadTableDB("level.json")["level"]
	end
	if numlevel == nil or numlevel > 3 then
		numlevel = 1
	end]]--
  local settings = require("libs.settings").getInstance()
  self.numlevel = settings:get("level")
  print("Num level"..self.numlevel)
  local fileNum = (self.numlevel % 3)
  print("loading background "..fileNum)
	local background = display.newImage("images/stages/0"..fileNum..".png")
  group:insert(background)
	background.x, background.y = (470 + kindlePadding), 300
	
	local sequenceData = { { name = "normalRun", start = 1, count = 42, time = 2000, loopCount = 0, loopDirection = "forward" } }
	local sequenceDataHappy = { { name = "normalRun",    start = 1, count = 70, time = 2000, loopCount = 0, loopDirection = "forward" } }
	local sequenceDataSad = { { name = "normalRun", start = 1, count = 59, time = 2000, loopCount = 0, loopDirection = "forward" } }

	local animation = display.newSprite( sheetImageCharacter, sequenceData )
  group:insert(animation)
	animation.y, animation.x = 200, 425
	animation:scale(1.5,1.5)
	animation:play()

  
	local animationHappy = display.newSprite( sheetImageCharacterHappy, sequenceDataHappy )
  group:insert(animationHappy)
	animationHappy.y, animationHappy.x = 200, 425
	animationHappy:scale(1.3,1.3)
	animationHappy.alpha = 0

	local animationSad = display.newSprite( sheetImageCharacterSad, sequenceDataSad )
  group:insert(animationSad)
	animationSad.y, animationSad.x = 200, 425
	animationSad:scale(1.3,1.3)
	animationSad.alpha = 0

	local initialFaja1 = display.newImage("images/commonresources/faja.png", -275, 337)
  group:insert(initialFaja1)
	local initialFaja2 = display.newImage("images/commonresources/faja.png", 780, 340)
  group:insert(initialFaja2)
	
	local table = display.newImage("images/commonresources/table.png")
  group:insert(table)
	table.x, table.y = (450 + kindlePadding), 560
	

	local tablecover = display.newImage("images/commonresources/tablecover.png", 960, 337)
  group:insert(tablecover)
	local btnleft = ui.newButton{
		defaultSrc = "images/buttons/button-left.png", defaultX = 100, defaultY = 100, overSrc = "images/buttons/button-left-over.png", overX = 100, overY = 100
	}
  group:insert(btnleft)
	btnleft.x, btnleft.y = -50, 715

	local btnright = ui.newButton{
		defaultSrc = "images/buttons/button-right.png", defaultX = 100,	defaultY = 110, overSrc = "images/buttons/button-right-over.png", overX = 100, overY = 110,
	}
	local btnnewcrepe = ui.newButton{
		defaultSrc = "images/commonresources/newcrepe.png", defaultX = 180,	defaultY = 110, overSrc = "images/buttons/button-right-over.png", overX = 100, overY = 110,
	}

  group:insert(btnright)
	btnright.x, btnright.y = 70, 715

	for i = 0, spaces do
		crepes[i] = Crepe:new(i,group)
	end
	crepes[0].tray.alpha, crepes[0].haveCrepe, crepes[0].crepe.alpha = 1, true, 1

	local icecreamAnimation = movieclip.newAnim({
		"images/icecreams/01/stage01-original0001.png","images/icecreams/01/stage01-original0002.png", "images/icecreams/01/stage01-original0003.png","images/icecreams/01/stage01-original0004.png","images/icecreams/01/stage01-original0005.png","images/icecreams/01/stage01-original0006.png","images/icecreams/01/stage01-original0007.png","images/icecreams/01/stage01-original0008.png","images/icecreams/01/stage01-original0009.png","images/icecreams/01/stage01-original0010.png",
		"images/icecreams/01/stage01-original0011.png","images/icecreams/01/stage01-original0012.png", "images/icecreams/01/stage01-original0013.png","images/icecreams/01/stage01-original0014.png","images/icecreams/01/stage01-original0015.png","images/icecreams/01/stage01-original0016.png","images/icecreams/01/stage01-original0017.png","images/icecreams/01/stage01-original0018.png","images/icecreams/01/stage01-original0019.png","images/icecreams/01/stage01-original0020.png",
		"images/icecreams/01/stage01-original0021.png","images/icecreams/01/stage01-original0022.png", "images/icecreams/01/stage01-original0023.png","images/icecreams/01/stage01-original0024.png","images/icecreams/01/stage01-original0025.png","images/icecreams/01/stage01-original0026.png","images/icecreams/01/stage01-original0027.png","images/icecreams/01/stage01-original0028.png","images/icecreams/01/stage01-original0029.png","images/icecreams/01/stage01-original0030.png",
		"images/icecreams/01/stage01-original0031.png","images/icecreams/01/stage01-original0032.png", "images/icecreams/01/stage01-original0033.png","images/icecreams/01/stage01-original0034.png","images/icecreams/01/stage01-original0035.png","images/icecreams/01/stage01-original0036.png","images/icecreams/01/stage01-original0037.png","images/icecreams/01/stage01-original0038.png","images/icecreams/01/stage01-original0039.png","images/icecreams/01/stage01-original0040.png",
		"images/icecreams/01/stage01-original0041.png","images/icecreams/01/stage01-original0042.png", "images/icecreams/01/stage01-original0043.png","images/icecreams/01/stage01-original0044.png","images/icecreams/01/stage01-original0045.png","images/icecreams/01/stage01-original0046.png","images/icecreams/01/stage01-original0047.png","images/icecreams/01/stage01-original0048.png","images/icecreams/01/stage01-original0049.png","images/icecreams/01/stage01-original0050.png",
		"images/icecreams/01/stage01-original0051.png","images/icecreams/01/stage01-original0052.png", "images/icecreams/01/stage01-original0053.png","images/icecreams/01/stage01-original0054.png","images/icecreams/01/stage01-original0055.png","images/icecreams/01/stage01-original0056.png","images/icecreams/01/stage01-original0057.png",
	}, 60, 20 )
	icecreamAnimation.x, icecreamAnimation.y = -7,270
	icecreamAnimation:scale(1.1, 0.86)
  group:insert(icecreamAnimation)
  group:insert(btnnewcrepe)
  btnnewcrepe.x, btnnewcrepe.y = 570, 715
  btnnewcrepe:scale(0.65,0.85)

  require('libs.orders')  
  local order=Order:new(self.numlevel)
	local function addIngredientAnimation(event)
		if event.phase == "ended" then
			Ingredients:verifyIngredient(event.target.id, event.target.column, crepes, icecreamAnimation,order,group)
      order:check(crepes,scene)
		end
	end

	local nline = 0
	local nrow = 0
	local count = 0
	local ingredients  = {}
	for i  = 0, 17 do
		ingredients[i] = Ingredients:new(i, nline, nrow, group )
		ingredients[i].ingredient:addEventListener("touch", addIngredientAnimation )
		ingredients[i].ingredientNameToShow:addEventListener("touch", addIngredientAnimation )
		count = count + 1
		nrow = nrow + 1
		if count == 3 then
			nline = nline + 1
			count = 0
			nrow = 0
		end
	end

	local creams  = {}
	for i  = 0, 4 do
		creams[i] = Ingredients:newCreams(i,group)
		creams[i].cream:addEventListener("touch", addIngredientAnimation )
	end

	local creamName1 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("darkchocolate"), -80, 000 ) 
  group:insert(creamName1)
	local creamName2 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("cream"), 100, 000 ) 
  group:insert(creamName2)
	local creamName3 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("vaillla"), 280, 000 ) 
  group:insert(creamName3)
	local creamName4 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("choloate"), 460, 000 ) 
  group:insert(creamName4)
	local creamName5 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("whitechocolate"), 640, 000 ) 
  group:insert(creamName5)

	local function toogleFreeze()
		isFreeze = false
	end
	local errorImage = display.newImage("images/commonresources/error.png")
  group:insert(errorImage)
	errorImage.alpha = 0
	local fantasticImage = display.newImage("images/commonresources/fantastic.png")
  group:insert(fantasticImage)
	fantasticImage.alpha = 0

	function btnright:touch(event)
		if event.phase == "ended" then
			if isFreeze == false then
				isFreeze = true
				Crepe:moveToRight(crepes, spaces, false, errorImage, animation, animationSad, animationHappy, fantasticImage,order,scene)
				--newTrayCounter = 0
				timer.performWithDelay(1001, toogleFreeze)
			end
		end
	end

	function btnleft:touch(event)
		if event.phase == "ended" then
			if isFreeze == false then
				isFreeze = true
				Crepe:moveToLeft(crepes, spaces)
				timer.performWithDelay(1001, toogleFreeze)
			end
		end
	end

	function btnnewcrepe:touch(event)
		if event.phase == "ended" then
			if isFreeze == false then
				isFreeze = true
				Crepe:visibleTheFirstCrepe()
	        	Crepe:moveToRight(crepes, spaces, true, errorImage, animation, animationSad, animationHappy, fantasticImage,order,scene)
	        	timer.performWithDelay(1001, toogleFreeze)
        	end
		end
	end

	local timmer = display.newImage("images/commonresources/timmer.png", 150, 630)
  group:insert(timmer)
	timmer:scale(0.5,0.7)

	local minutes = display.newText("2:",300,670,"Comic Sans MS",50);
  group:insert(minutes)
	local seconds = display.newText("00",340,670, "Comic Sans MS",50)
  group:insert(seconds)
	self.minutesNumber = 2
	self.secondsNumber = 0
  
	minutes:setTextColor(0, 0, 0)

	seconds:setTextColor(0, 0, 0)

	local updateTime = function(event)
    if (self.gameIsPaused == false) then
      if(self.secondsNumber == 1 and self.minutesNumber == 0) then
        seconds.text = ("00".."");
        scene:finishLevel("fail")
        
      else
        if self.secondsNumber == 0 then
          self.minutesNumber = self.minutesNumber - 1;
          minutes.text = (self.minutesNumber..":");
          self.secondsNumber = 59;
          seconds.text = (self.secondsNumber.."");
          
        else
          self.secondsNumber = self.secondsNumber - 1;
          seconds.text = (self.secondsNumber.."");
          if self.secondsNumber < 10 then
            seconds.text = "0"..seconds.text
          end
        end
      end
		
      --newTrayCounter = newTrayCounter + 1
      --if newTrayCounter > 14 and isFreeze == false then
        --isFreeze = true
        --Crepe:visibleTheFirstCrepe()
        --Crepe:moveToRight(crepes, spaces, true, errorImage, animation, animationSad, animationHappy, fantasticImage,order,scene)
        --timer.performWithDelay(1001, toogleFreeze)
        --newTrayCounter = 0
      --end
    end	
	end



	self.updatingTimer = timer.performWithDelay( 1000, updateTime, 120 )

  
	order:display(group)
  --Set return to menu
      self.menuBtn = display.newImage("images/buttons/btn_return_menu.png")
      self.menuBtn:scale(.6,.6)
      group:insert(self.menuBtn)
      self.menuBtn.x = 1060
      self.menuBtn.y = 714
      
      --Set return to menu text
      --local menuText = display.newText("Pause", 1050,689, "Helvetica",32)
      --menuText:setTextColor(20, 20, 20)
      --group:insert(menuText)
    
       
      function self.menuBtn:touch(event)
        scene:pauseGame()
      end
      self.menuBtn:addEventListener( "touch", self.menuBtn )

	--self:finishLevel("loose")
	--self:finishLevel("loose")
  --storyboard.gotoScene("main_menu.main")
  --storyboard.hideOverlay()
  --self:pauseGame()
	
end



-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
  local function cancelTimer()
    timer.cancel(self.updatingTimer)
  end
  if pcall(cancelTimer) == false then
    print("cancelTimer glitch")
  end
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	
end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end


---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene