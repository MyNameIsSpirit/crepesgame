local storyboard = require "storyboard"
local widget = require( "widget" )

 
HighScores={}



function HighScores:new()
  -- gameType debe de ser "main" o "mini"
  
  
  local settings = require("libs.settings").getInstance()
  
  
  
  function self:get(gameType,position)
    local score = settings:get(gameType .. "Scores")
    return score[position]
  end
  function self:pushNew(gameType,value,name)
    local scores = settings:get(gameType .. "Scores")
    local temp = nil
    local temp2 = {}
    temp2.name = ""
    temp2.value = ""
    local replaced = -1
    for i = 1, 5  do
      local elem = scores[i]
      
      if tonumber(elem.value) < value and not temp then
        temp = {}
        temp.name = elem.name
        temp.value = elem.value
        elem.name = name
        elem.value = value
        replaced = i
      end
      if temp and i < 5 then
        temp2.name = scores[i+1].name
        temp2.value = scores[i+1].value
        scores[i+1].name = temp.name
        scores[i+1].value = temp.value
        temp.name = temp2.name
        temp.value = temp2.value
      end
        
    end
    settings:set(gameType .. "Scores",scores)
    return replaced
  end
  function self:clear(gameType)
    settings:set(gameType .. "Score",HighScores:generate())
  end 
  
  function self:setActualScore(gameType,value)
    
    settings:set(gameType.."ActualScore",value)
    self:updateDisplay(gameType)
  end
  
  function self:getActualScore(gameType)
    if not gameType then
      gameType = "main"
    end
    return settings:get(gameType.."ActualScore")
  end
  
  function self:updateDisplay(gameType)
    if self.levelText then
      local actualScore = self:getActualScore(gameType)
      if not actualScore then
        self:setActualScore(gameType,0)
      end
      self.levelText.text = ""..self:getActualScore(gameType)
    end
  end
  
  return self
end

function HighScores:generate()
  local sampleScore = {}
  sampleScore[1],sampleScore[2],sampleScore[3],sampleScore[4],sampleScore[5] = {},{},{},{},{}
  sampleScore[1].name = "Brain"
  sampleScore[1].value = "500"
  sampleScore[2].name = "Pinky"
  sampleScore[2].value = "400"
  sampleScore[3].name = "Dag"
  sampleScore[3].value = "300"
  sampleScore[4].name = "Norb"
  sampleScore[4].value = "200"
  sampleScore[5].name = "Chef"
  sampleScore[5].value = "100"
  return sampleScore
end

function HighScores:display(group)
    local mainScores = HighScores:new("main")
    local miniScores = HighScores:new("mini")
    --local group = scene.view
    -- Ubicando background
    local background = display.newImage("images/orders/blackboard.png",0,0)
    background:scale(1.25,1.2)
    background.x = 515
    background.y = 370
    group:insert(background)
    --local scoresTitle = display.newText("High Scores!",350,49,"Helvetica",55);
    --scoresTitle:setTextColor( 100, 200, 200, 230 )
    --group:insert(scoresTitle)
    
    local mainScoreAlignX = 125
    local scoresSpaceY = 90
    local miniScoreAlignX = 700
    
    
    local mainScoreD = display.newText("Best crepe makers!",mainScoreAlignX,75,"Comic Sans MS",42);
    mainScoreD:setTextColor( 250, 250, 0, 250 )
    group:insert(mainScoreD)
    local miniScoreD = display.newText("Best catchers!",miniScoreAlignX+16,75,"Comic Sans MS",42);
    miniScoreD:setTextColor( 250, 250, 0, 250 )
    group:insert(miniScoreD)
    
    local mainCoin = display.newImage("images/commonresources/Coin.png",mainScoreAlignX - 120,75+scoresSpaceY-120)
      mainCoin:scale(0.7,0.7)
      mainCoin.alpha = 0.9
      group:insert(mainCoin)
      local miniCoin = display.newImage("images/commonresources/Coin.png",miniScoreAlignX - 120,75+scoresSpaceY-120)
      miniCoin:scale(0.7,0.7)
      miniCoin.alpha = 0.9
      group:insert(miniCoin)
    
    for i=1,5 do

      --Scores
      local mainScore = mainScores:get("main",i)
      local mainNumber = display.newText(""..mainScore.value,mainScoreAlignX - 50,75+scoresSpaceY*i,"Comic Sans MS",40);
      mainNumber:setTextColor( 250, 250, 0, 250 )
      group:insert(mainNumber)  
      local mainName = display.newText(""..mainScore.name,mainScoreAlignX + 110,75+scoresSpaceY*i,"Comic Sans MS",40);
      mainName:setTextColor( 250, 250, 250, 250 )
      group:insert(mainName) 
      
      local miniScore = miniScores:get("mini",i)
      local miniNumber = display.newText(""..miniScore.value,miniScoreAlignX - 50,75+scoresSpaceY*i,"Comic Sans MS",40);
      miniNumber:setTextColor( 250, 250, 0, 250 )
      group:insert(miniNumber)
      local miniName = display.newText(""..miniScore.name,miniScoreAlignX + 110,75+scoresSpaceY*i,"Comic Sans MS",40);
      miniName:setTextColor( 250, 250, 250, 250 )
      group:insert(miniName)

     
    end
    
    --Agregando boton de salida
    local function hide()
      storyboard.hideOverlay()
    end
    local function hideOverlay(event)
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
    end
    local closeScoresB = widget.newButton
    {
        left = 340,
        top = 615,
        width = 320,
        height = 60,
        defaultFile = "images/commonresources/timmer.png",
        --overFile = "over.png",
        id = "button_1",
        label = "Close",
        onEvent = hideOverlay
    }
    
    
    
    
    group:insert(closeScoresB)
  end

return HighScores