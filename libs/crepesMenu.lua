local CrepesMenu={}



function CrepesMenu:new()
  local menu={}
  local types={}
  local levels={}
  setmetatable(types, {__index=table})
  types["cookiesAndCreamCrepe"] = {"", "chocolate","cookies","vainilla"}
  --types["amandineCrepe"] = {"", "", "almonds","maple"}
  --types["boardwalkCrepe"] = {"","onions","salt","slicedsausages"}
  types["chocolateCrepe"] = {"", "", "darkchocolate","whitechocolate"}
  --types["americanApplePieCrepe"] = {"", "apples", "icecream","maple"}
  types["danesDreamCrepe"] = {"", "cheese","ham","tomatoes"}
  --types["strawberryShortcakeCrepe"] = {"cookies","strawberries","strawberriesCream","whippedcream"}
  types["clubhouseCrepe"] = {"bacon","cheese","mushrooms","tomatoes"}
  
  levels[1] = { cookiesAndCreamCrepe = 1, danesDreamCrepe = 1, chocolateCrepe = 1 }
  levels[2] = { cookiesAndCreamCrepe = 1, clubhouseCrepe = 1, danesDreamCrepe = 1}
  levels[3] = { danesDreamCrepe = 1, chocolateCrepe = 1, clubhouseCrepe = 1}
  levels[4] = { cookiesAndCreamCrepe = 2, chocolateCrepe = 3, danesDreamCrepe = 1}
  levels[5] = { danesDreamCrepe = 2, clubhouseCrepe = 1, chocolateCrepe = 1}
  levels[6] = { cookiesAndCreamCrepe = 2, clubhouseCrepe = 1, chocolateCrepe = 1}
  levels[7] = { danesDreamCrepe = 2, clubhouseCrepe = 1, chocolateCrepe = 1}
  levels[8] = { clubhouseCrepe = 1, chocolateCrepe = 1, danesDreamCrepe = 1}
  levels[9] = { clubhouseCrepe = 1, danesDreamCrepe = 2, chocolateCrepe = 1}
  levels[10] = { cookiesAndCreamCrepe = 1, clubhouseCrepe = 2, chocolateCrepe = 1}
  
  menu.types=types
  menu.levels=levels
  
  
  function menu:getIngredients(crepeType)
    local ingredientNames=menu.types[crepeType]
    local ingredients={}
    setmetatable(ingredients, {__index=table})
    if ingredientNames ~= nil then
      for k, v in pairs(ingredientNames) do
        --if(v ~= "") then
          ingredients:insert(Ingredients:findByName(v))
        --end
      end
    end
    return ingredients
  end
  
  
  
  function menu:getTypes(level)
    print("Got level 1, returning" )
    return menu.levels[level]
  end
  return menu
end

return CrepesMenu:new()