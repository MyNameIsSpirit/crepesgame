local initialXPosition = -215
local finalXPosition = 915
local defaulImg = "images/commonresources/default.png"

eimage = ""
idle = ""
sad = ""
happy = ""
fantastic = ""

Crepe = {}
function Crepe:new(Id,group)
	local c = {}
	c.id = Id
	c.position = c.id
	c.faja = display.newImage("images/commonresources/faja.png")
  group:insert(c.faja)
	c.tray = display.newImage("images/commonresources/tray.png")
  group:insert(c.tray)
	c.crepe =  display.newImage("images/commonresources/crepe.png")
  group:insert(c.crepe)
	c.faja.x, c.tray.x, c.crepe.x = ( c.id * 190 ) -215, ( c.id * 190 ) -215, ( c.id * 190 ) -215
	c.faja.y, c.tray.y, c.crepe.y = 405, 390, 390
	c.haveCrepe = false
	c.crepe.alpha, c.tray.alpha = 0, 0
	c.ingredient1 = display.newImage(defaulImg, c.tray.x, c.tray.y)
  group:insert(c.ingredient1)
 	c.ingredient2 = display.newImage(defaulImg)
  group:insert(c.ingredient2)
 	c.ingredient3 = display.newImage(defaulImg)
  group:insert(c.ingredient3)
 	c.ingredient4 = display.newImage(defaulImg)
  group:insert(c.ingredient4)
 	c.ingredient1.src = (defaulImg)
 	c.ingredient2.src = (defaulImg)
 	c.ingredient3.src = (defaulImg)
 	c.ingredient4.src = (defaulImg)
 	c.ingredient1.name = ""
 	c.ingredient2.name = ""
 	c.ingredient3.name = ""
 	c.ingredient4.name = ""
  c.outOfLine = false
  function c:isAtEnd()
    --Si outOfLine es nil, pasa a true y pasa a false
    return  not not self.outOfLine
  end
  function c:hasIngredient(ingredientName)
    if c.ingredient1.name == ingredientName then
      return true
    elseif c.ingredient2.name == ingredientName then
      return true
    elseif c.ingredient3.name == ingredientName then
      return true
    elseif c.ingredient4.name == ingredientName then
      return true
    end
  end
	return c
end

function Crepe:moveToRight(crepes, spaces, isByTimmer, errorImage, idleAnimation, sadAnimation, happyAnimation, fantasticImage,orderWrapper,scene)
  local group = scene.view
	eimage = errorImage
	idle = idleAnimation
	sad = sadAnimation
	happy = happyAnimation
	fantastic = fantasticImage
	for i = 0, spaces do 
		if crepes[i].faja.x < 915 then
			transition.to(crepes[i].faja, {time=1000,  x= crepes[i].faja.x + 190 })
			transition.to(crepes[i].tray, {time=1000,  x= crepes[i].tray.x + 190 })
			transition.to(crepes[i].crepe, {time=1000,  x= crepes[i].crepe.x + 190 })
			crepes[i].position = crepes[i].position + 1
			transition.to(crepes[i].ingredient1, {time=1000,  x= crepes[i].ingredient1.x + 190 })
			transition.to(crepes[i].ingredient2, {time=1000,  x= crepes[i].ingredient2.x + 190 })
			transition.to(crepes[i].ingredient3, {time=1000,  x= crepes[i].ingredient3.x + 190 })
			transition.to(crepes[i].ingredient4, {time=1000,  x= crepes[i].ingredient4.x + 190 })
		else
			local order = crepes[i]
      --La marcamos como fuera de la linea
      crepes[i].outOfLine = true
			if order.haveCrepe == true then
				isAValidOrder(order, errorImage,orderWrapper,crepes,scene)
			end 
			crepes[i].tray.alpha, crepes[i].crepe.alpha, crepes[i].ingredient1.alpha,
      crepes[i].ingredient2.alpha, crepes[i].ingredient3.alpha, crepes[i].ingredient4.alpha = 0, 0, 0, 0, 0, 0
      crepes[i].outOfLine = false
			crepes[i].ingredient1.name = ""
			crepes[i].ingredient2.name = ""
			crepes[i].ingredient3.name = ""
			crepes[i].ingredient4.name = ""
			crepes[i].ingredient1 = display.newImage(defaulImg)
      group:insert(crepes[i].ingredient1)
			crepes[i].ingredient2 = display.newImage(defaulImg)
      group:insert(crepes[i].ingredient2)
			crepes[i].ingredient3 = display.newImage(defaulImg)
      group:insert(crepes[i].ingredient3)
			crepes[i].ingredient4 = display.newImage(defaulImg)
      group:insert(crepes[i].ingredient4)
			crepes[i].position = 0
			crepes[i].tray.x, crepes[i].faja.x, crepes[i].crepe.x = initialXPosition, initialXPosition, initialXPosition
			crepes[i].haveCrepe = false
			
		end
	end
	--if isByTimmer == false then 
	--	evaluateNewCrepe(crepes, spaces)
	--end
end


function Crepe:moveToLeft(crepes, spaces)
	local temp1 = nil;
	local temp2 = nil;
	for i = 0, spaces do 
		if crepes[i].position == 0 then
			temp1 = crepes[i]
		end
		if crepes[i].position == 1 then
			temp2 = crepes[i]
		end
	end 

	if temp1 ~= nil and temp1.haveCrepe ~= nil and temp1.haveCrepe == false and temp2.haveCrepe == false then
		for i = 0, spaces do 
			if crepes[i].position > 0 then
				transition.to(crepes[i].faja, {time=1000,  x= crepes[i].faja.x - 190 })
				transition.to(crepes[i].tray, {time=1000,  x= crepes[i].tray.x - 190 })
				transition.to(crepes[i].crepe, {time=1000,  x= crepes[i].crepe.x - 190 })
				crepes[i].position = crepes[i].position - 1
				transition.to(crepes[i].ingredient1, {time=1000,  x= crepes[i].ingredient1.x - 190 })
				transition.to(crepes[i].ingredient2, {time=1000,  x= crepes[i].ingredient2.x - 190 })
				transition.to(crepes[i].ingredient3, {time=1000,  x= crepes[i].ingredient3.x - 190 })
				transition.to(crepes[i].ingredient4, {time=1000,  x= crepes[i].ingredient4.x - 190 })
			else
				crepes[i].tray.x, crepes[i].crepe.x, crepes[i].faja.x = finalXPosition, finalXPosition, finalXPosition
				crepes[i].position = spaces
			end
		end
	end

end



function Crepe:visibleTheFirstCrepe()
	for i = 0, spaces do
		if crepes[i].position == 0 then
			crepes[i].haveCrepe = true
			crepes[i].tray.alpha = 1
			crepes[i].crepe.alpha = 1
		end
	end
end


function hideErrorImage()
	eimage.alpha = 0
	idle.alpha = 1
	sad:pause()
	sad.alpha = 0
end

function hideHappyAnimation()
	fantastic.alpha = 0
	idle.alpha = 1
  local function pauseHappy()
    happy:pause()
  end
  if pcall(pauseHappy) == false then
    print("failed fail")
  end
	
	happy.alpha = 0
end

function isAValidOrder(crepeSuccess, errorImage,order,crepes,scene)
	isCorrect = false
	
	isCorrect = order:check(crepes,scene)

	if (isCorrect == false) then
		errorImage.alpha = 1		
		errorImage.x = 450
		errorImage.y = 310
		idle.alpha = 0
		sad.alpha = 1
		sad:play()
		timer.performWithDelay(2000, hideErrorImage )
	else
		fantastic.alpha = 1		
		fantastic.x = 150
		fantastic.y = 310
		idle.alpha = 0
		happy.alpha = 1
		happy:play()
		timer.performWithDelay(2000, hideHappyAnimation )
	end
end

return Crepe