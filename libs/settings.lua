
local json = require("json")
local Files = require "libs.files"
local HighScores = require "libs.scores"

Settings={}
 
local _settingsInstance

 
function Settings:new()
    print('Singleton cannot be instantiated - use getInstance() instead');
end

function Settings.getInstance()
  if not _settingsInstance then
    _settingsInstance = Settings;
    
    _settingsInstance.data = {}
    function _settingsInstance:set(key,value)
      _settingsInstance.data[key] = value
      Files:saveTableDB(_settingsInstance.data, "settings.json")
    end
    function _settingsInstance:get(key)
      --Not thread safe, should not relay in saved state
      _settingsInstance.data = Files:loadTableDB("settings.json")
      return _settingsInstance.data[key]
    end
    if Files:loadTableDB("settings.json") ~= nil then
      _settingsInstance.data = Files:loadTableDB("settings.json")
    else
      _settingsInstance:set("level",1)
      _settingsInstance:set("music",true)
      _settingsInstance:set("sound",true)
      local mainScores = HighScores:generate()
      _settingsInstance:set("mainScores",mainScores)
      local miniScores = HighScores:generate()
      _settingsInstance:set("miniScores",miniScores)
      _settingsInstance:set("mainActualScore",0)
      _settingsInstance:set("miniActualScore",0)
    end
  end
 
  --'any new methods would be added to the _instance object like this'
  _settingsInstance.getType = function()
    return 'settings';
  end
  
  return _settingsInstance
end



return Settings
