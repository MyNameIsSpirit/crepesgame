Functions = {}

function Functions:deepcompare(t1,t2,ignore_mt)
	local ty1 = type(t1)
	local ty2 = type(t2)
	if ty1 ~= ty2 then return false end
	if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
	local mt = getmetatable(t1)
	if not ignore_mt and mt and mt.__eq then return t1 == t2 end
	for k1,v1 in pairs(t1) do
		local v2 = t2[k1]
		if v2 == nil or not deepcompare(v1,v2) then return false end
	end
	for k2,v2 in pairs(t2) do
		local v1 = t1[k2]
		if v1 == nil or not deepcompare(v1,v2) then return false end
	end
	return true
end

function Functions:sortIngredients(i1, i2, i3, i4)
	if i1 == nil then i1 = "" end
	if i2 == nil then i2 = "" end
	if i3 == nil then i3 = "" end
	if i4 == nil then i4 = "" end
			
	local order = { i1, i2, i3, i4}

	bubbleSort(order)
	local orderSorted = {}
	for i, j in pairs(order) do
	    orderSorted[i] = order[i]
	end
	return orderSorted
end

function bubbleSort(A)
	local itemCount=#A
  	local hasChanged
  	repeat
    	hasChanged = false
    	itemCount=itemCount - 1
    for i = 1, itemCount do
    	if A[i] > A[i + 1] then
        	A[i], A[i + 1] = A[i + 1], A[i]
        	hasChanged = true
      	end
    end
  	until hasChanged == false
end



return Functions
