local sheetInfoCharacter = require("images.idle.idle")
local sheetImageCharacter = graphics.newImageSheet("images/idle/idle.png",sheetInfoCharacter:getSheet())

local sheetInfoIngredients = require("images.ingredients.ingredients")
local sheetImageIngredients = graphics.newImageSheet("images/ingredients/ingredients.png",sheetInfoIngredients:getSheet())


Character = {}

function Character:idle(Id)
	local c = {}
	c = display.newImage( sheetImageIngredients ,sheetInfoIngredients:getFrameIndex(Id.."") ) 
	--i.ingredient = display.newImage( sheetImageIngredients ,sheetInfoIngredients:getFrameIndex(Id.."") ) 
	return c
end

return Character
