local sheetInfoIngredients = require("images.ingredients.ingredients")
local sheetImageIngredients = graphics.newImageSheet("images/ingredients/ingredients.png",sheetInfoIngredients:getSheet())
local errorSound = audio.loadStream("audio/error.mp3")
local HighScores = require "libs.scores"
local mainScores = HighScores:new("main")

Order = {}

function Order:new(level)
    local o = {}
    local menu = require('libs.crepesMenu')
    local crepesCount = menu:getTypes(level)
    o.crepesToMake = {}
    setmetatable(o.crepesToMake, {__index=table})
    for k, v in pairs(crepesCount) do
      local crepeDetail={}
      crepeDetail.count = v
      crepeDetail.name = k
      
      crepeDetail.ingredients = menu:getIngredients(k)
      crepeDetail.ingredientsCount = 0
      
      
      
      for k,v in pairs(crepeDetail.ingredients) do
        if v.name ~= "" then
          crepeDetail.ingredientsCount = crepeDetail.ingredientsCount + 1
        end
      end
      
      crepeDetail.maxCorrectIngredients = 0
      
      function crepeDetail:belongsToCrepe(ingredientName,crepeName)
        local ingredients=self.ingredients
        if ingredientName == nil then
          ingredientName = ""
        end
        for k,v in pairs(ingredients) do
          if ingredientName == v.name then
            return v
          end
        end
        if ingredientName == "" then
            return nil            
        end
        return false
      end
      
      function crepeDetail:resetImages()
        for v,k in pairs(self.ingredients) do
          if k.name ~= "" then
            k.image.alpha = 1	    
          end
        end
      end
      
      function crepeDetail:reset()
        self.maxCorrectIngredients=0
        self:resetImages()
      end
      
      
      
      function crepeDetail:applyTransparencies(correctIng,correctCount)
        if (correctCount>=self.maxCorrectIngredients) then
          self:reset()
          for v,k in pairs(correctIng) do
            if k and k.name ~= "" then
              k.image.alpha = 0.4
            end
          end
          self.maxCorrectIngredients=correctCount
        end
      end
      
      --checkCrepe debe de conocer si esta al final de la linea
      --  si esta al final de la linea ya no debe de esperar por mas ings si es 
      --  crepa de 4 y hay nils
      function crepeDetail:checkCrepe(crepe,endOfLine)
        local correctIng = {}
        correctIng[1]=self:belongsToCrepe(crepe.ingredient1.name,crepe)
        correctIng[2]=self:belongsToCrepe(crepe.ingredient2.name,crepe)
        correctIng[3]=self:belongsToCrepe(crepe.ingredient3.name,crepe)
        correctIng[4]=self:belongsToCrepe(crepe.ingredient4.name,crepe) 
	
        local correctCount=0
	
        --nil = a que "" no pertenece a los ings
        if endOfLine and correctIng[4] == nil then --Hay nils y trues, pero es el final de la linea, no podemos esperar por mas ingreds
            return -1
        end
        
        -- Los ingredientes vacios siempre son los ultimos
        -- por tanto dentro del bucle nunca pasan nils
        for v,k in pairs(correctIng) do
          if k == false then
            return -1
          else
            --es true y no es vacio, un ing correcto!
            if k.name ~= "" then
              correctCount=correctCount+1
            end
          end
        end
        --Aplicar transparencias o intentarlo con nuevo num
        self:applyTransparencies(correctIng,correctCount)
        return correctCount
      end
      
      print("crepeDetail: " .. crepeDetail.name)
      o.crepesToMake:insert(crepeDetail)
    end
    
    function o:display(group)
      local blackboard=display.newImage("images/orders/blackboard.png")
      group:insert(blackboard)
      blackboard:scale(0.42,0.52)
      blackboard.x=990
      blackboard.y=165
      local ordersLabel = display.newText("Orders!",900,25,"Comic Sans MS",31);
      group:insert(ordersLabel)
      ordersLabel:setTextColor( 250, 250, 250, 230 )
      local x
      local y
      for i,crepeDetail in pairs(o.crepesToMake) do
        print("displaying ingredients for crepe "..crepeDetail.name)
        local ingredientsToDisplay={}
        setmetatable(ingredientsToDisplay, {__index=table})
        for k,v in pairs(crepeDetail.ingredients) do
          if v.name ~= "" then
            ingredientsToDisplay:insert(v)
          end
        end
        for k, v in pairs(ingredientsToDisplay) do
          if v.name then
            print("displaying ingredient "..v.id.."  ".. v.name)
            x=k*70 + 750
            y=i*80 + -10
            v.image=display.newImage( sheetImageIngredients ,sheetInfoIngredients:            getFrameIndex(v.id..""),x,y )
            group:insert(v.image)
          end
        end
        --count
        crepeDetail.countText=display.newText("x"..crepeDetail.count,x+80,y-7,"Comic Sans MS",34);
        group:insert(crepeDetail.countText)
        crepeDetail.countText:setTextColor( 225, 00, 00 )
        --name
        crepeDetail.nameText=display.newText(crepeDetail.name,820,y+36,"Comic Sans MS",22);
        group:insert(crepeDetail.nameText)
        crepeDetail.nameText:setTextColor( 200, 200, 200, 230 )
      end
      -- Display Score counter
      local timmer = display.newImage("images/commonresources/timmer.png", 650, 635)
      group:insert(timmer)
      timmer:scale(0.5,0.7)
      local coin = display.newImage("images/commonresources/coin.png", 700, 650)
      group:insert(coin)
      coin:scale(0.75,0.75)
      
      
      
      mainScores.levelText = display.newText(""..0, 861,670, "Comic Sans MS",50)
      mainScores.levelText:setTextColor(0, 0, 0)
      group:insert(mainScores.levelText)
      --hay un problema con el despliegue del texto al inicializar se desplaza
      --asi que se inicia como 0 y se obliga a actualizarse
      mainScores:setActualScore("main",mainScores:getActualScore("main"))
      
    end
    
    
    function o:check(crepesLine,scene)
      print("starting check")
      local crepes={}
      local result=false
      setmetatable(crepes, {__index=table})
      for m=0,6 do
        local i=crepesLine[m]
        if i.haveCrepe then
          
          crepes:insert(i)
        end
      end
      
      for v,crepeDetail in pairs(self.crepesToMake) do
        crepeDetail:reset()    
      end
      
      local endOfLine = false
      --for k = 0, 6 do
        --v=crepes[k]
      for k,v in pairs(crepes) do
        --Esta la crepa al final de la linea?
        endOfLine = endOfLine or v:isAtEnd()
        print("*******************************")
        print("crepe ingredients: ")
        if v.ingredient1 then
          print(v.ingredient1.name)
        end
        if v.ingredient2 then
          print(v.ingredient2.name)
        end
        if v.ingredient3 then
          print(v.ingredient3.name)
        end
        if v.ingredient4 then
          print(v.ingredient4.name)
        end
        
       	local crepeIsOut = v:isAtEnd()
        --Variable para verificar si se cumplieron todas
        local orderComplete = true
        for i,crepeDetail in pairs(self.crepesToMake) do
          
          
          --[[print("-----------------")
          print("detail: ")
          if self.crepesToMake then
            for o,p in pairs(crepeDetail.ingredients) do
              print(p.name)
            end
          end]]--
          if crepeDetail.count > 0 then
            local correctIngredients = crepeDetail:checkCrepe(v,v:isAtEnd())
            if crepeIsOut then
              --Si es correcto
              if correctIngredients > -1 and crepeDetail.ingredientsCount == correctIngredients then
                crepeDetail.count = crepeDetail.count - 1
                crepeDetail.countText.text = "x"..crepeDetail.count
                result=true
                --Puede que quede otra crepa con el orden bueno y no muestre transparencias para esta
                crepeDetail:reset()
                mainScores:setActualScore("main",mainScores:getActualScore("main")+25*correctIngredients)
                print("Valid crepe!")
              end      
            end
          end
          orderComplete = orderComplete and crepeDetail.count == 0
        end
        if orderComplete then
          print("ORDER COMPLETE!")
          scene:finishLevel("win")          
        end
      end
      
      if result == false and endOfLine then
        print("error")
        audio.play(errorSound, {channel = 4, loops = 1} )
      end
      
      
      
      
      return result
    end 
    

    return o
end