local sheetInfoIngredients = require("images.ingredients.ingredients")
local sheetImageIngredients = graphics.newImageSheet("images/ingredients/ingredients.png",sheetInfoIngredients:getSheet())

local sheetInfoCreams = require("images.creams.creams")
local sheetImageCreams = graphics.newImageSheet("images/creams/creams.png",sheetInfoCreams:getSheet())
local kitchanSound = ""

local kitchenSound = audio.loadStream("audio/kitchen.mp3")
local errorSound = audio.loadStream("audio/error.mp3")

  local kindlePadding = 0;

  if (system.getInfo("model") == "Kindle Fire" or system.getInfo("model") == "KFJWI" or system.getInfo("model") == "KFTT" or system.getInfo("model") == "BNRV200") then
   kindlePadding = 70
  end

Ingredients = {}

function Ingredients:new(Id, column, row,group)
	local i = {}
	i.id = Id
	i.ingredient = display.newImage( sheetImageIngredients ,sheetInfoIngredients:getFrameIndex(Id.."") ) 
  group:insert(i.ingredient)
	i.ingredient.x, i.ingredient.y = (column * 185) -123 + kindlePadding, (60 * row) + 500
	i.ingredient.id = Id
	i.ingredientName = ingredientNames[Id+1]
	i.ingredient.column = column
	i.ingredientNameToShow = display.newText(string.upper(ingredientNames[Id+1]), (column * 185) -90 + kindlePadding, (65 * row) + 490, "Comic Sans MS",15)
  group:insert(i.ingredientNameToShow)
	i.ingredientNameToShow.id = Id
	i.ingredientNameToShow.column = column
	i.ingredientNameToShow:setTextColor(0, 0, 0)
	return i
end

function Ingredients:findByName(name)
  	local i={}
  	for k, v in pairs(ingredientNames) do    
      if(name == v) then      
      		i.id = k - 1
      		i.name = v	
    	end
      if name == "" then
        i.id=""
        i.name=""
      end
  	end
  	return i
end

function Ingredients:newCreams(Id,group)
	local i = {}
	i.id = Id + 19
	i.cream = display.newImage( sheetImageCreams ,sheetInfoCreams:getFrameIndex(Id.."") ) 
  group:insert(i.cream)
	i.cream.x, i.cream.y = ( Id * 180 ), 85
	i.ingredientName = ingredientNames[Id+1]
	i.cream.column = Id
	i.cream.id = i.id
	return i
end

function Ingredients:verifyIngredient(id, column, crepes, creamAnimation, kitchenSound,group)
	kitchanSound = kitchenSound
	numIngredient = id
	numColumn = column + 1
	foundElement = false
	for i = 0,6 do
    --Verificando si el ingrediente esta repetido
    local newIngredient= not crepes[i]:hasIngredient(ingredientNames[numIngredient + 1])
		if crepes[i].haveCrepe == true and crepes[i].position == numColumn then
			if crepes[i].ingredient1.name == "" or crepes[i].ingredient1.name == nil  then
        
        if newIngredient then
          crepes[i].ingredient1 = display.newImage(sheetImageIngredients ,
            sheetInfoIngredients:getFrameIndex(numIngredient..""), crepes[i].tray.x, crepes[i].tray.y  -20)
          group:insert(crepes[i].ingredient1)
          crepes[i].ingredient1.src = ""
          crepes[i].ingredient1.name = ingredientNames[numIngredient + 1]
        end
				audio.play(kitchanSound, {channel = 3, loops = 1 } )
				foundElement = true
				if id > 18 then
					local temp = ( ( ( id - 19 ) * 183) - 7 )
					creamAnimation.x = temp
					creamAnimation:play{ startFrame=1, endFrame=57, loop=1, remove=false }
				else
					timer.performWithDelay(0, kitchenIngredient )
				end
			else
				if crepes[i].ingredient2.name == "" or crepes[i].ingredient2.name == nil  then
          if newIngredient then
            crepes[i].ingredient2 = display.newImage(sheetImageIngredients ,
              sheetInfoIngredients:getFrameIndex(numIngredient..""), crepes[i].tray.x, crepes[i].tray.y  -20)
            group:insert(crepes[i].ingredient2)
            crepes[i].ingredient2.src = ""
            crepes[i].ingredient2.name = ingredientNames[numIngredient + 1]
          end
					audio.play(kitchanSound, {channel = 3, loops = 1 } )
					foundElement = true
					if id > 18 then
						local temp = ( ( ( id - 19 ) * 183) - 7 )
						creamAnimation.x = temp
						creamAnimation:play{ startFrame=1, endFrame=57, loop=1, remove=false }
					else
						timer.performWithDelay(0, kitchenIngredient )
					end
				else
					if crepes[i].ingredient3.name == "" or crepes[i].ingredient3.name == nil  then
            if newIngredient then
              crepes[i].ingredient3 = display.newImage(sheetImageIngredients ,
                sheetInfoIngredients:getFrameIndex(numIngredient..""), crepes[i].tray.x, crepes[i].tray.y  -20)
              group:insert(crepes[i].ingredient3)
              crepes[i].ingredient3.src = ""
              crepes[i].ingredient3.name = ingredientNames[numIngredient + 1]
            end
						audio.play(kitchanSound, {channel = 3, loops = 1 } )
						foundElement = true
						if id > 18 then
							local temp = ( ( ( id - 19 ) * 183) - 7 )
							creamAnimation.x = temp
							creamAnimation:play{ startFrame=1, endFrame=57, loop=1, remove=false }
						else
							timer.performWithDelay(0, kitchenIngredient )
						end
					else
						if crepes[i].ingredient4.name == "" or crepes[i].ingredient4.name == nil  then
              if newIngredient then
                crepes[i].ingredient4 = display.newImage(sheetImageIngredients ,
                  sheetInfoIngredients:getFrameIndex(numIngredient..""), crepes[i].tray.x, crepes[i].tray.y  -20)
                group:insert(crepes[i].ingredient4)
                crepes[i].ingredient4.src = ""
                crepes[i].ingredient4.name = ingredientNames[numIngredient + 1]
              end
							audio.play(kitchanSound, {channel = 3, loops = 1 } )
							foundElement = true
							if id > 18 then
								local temp = ( ( ( id - 19 ) * 183) - 7 )
								creamAnimation.x = temp
								creamAnimation:play{ startFrame=1, endFrame=57, loop=1, remove=false }
							else
								timer.performWithDelay(0, kitchenIngredient )
							end
						else
							timer.performWithDelay(1000, errorSound )
						end
					end
				end
			end
		end
	end
	if foundElement == false then
		timer.performWithDelay(1000, errorSound )
	end
	return true
end

function kitchenIngredient()
	audio.play(kitchenSound, {channel = 3, loops = 1} )
end

function errorSound()
	audio.play(errorSound, {channel = 3, loops = 2} )
end

ingredientNames = {"cherries","almonds","mushrooms",
		    "cookies",
		    "mani","strawberries","sausages",
		    "turkey","ham","mandarin","bannanas",
		    "cheese","cookies","tomatoes",
		    "bacon","apples","mayonnaise","salt",
		    "cream","chocolate","vainilla","darkchocolate","whitechocolate"
				}

return Ingredients