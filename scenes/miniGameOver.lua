local storyboard = require "storyboard"

local scene = storyboard.newScene()

local function catchAllTheThings() --!
  return true
end

function scene:createScene( event )
  storyboard.removeScene("scenes.scores")
  local HighScores = require "libs.scores"
  local miniScores = HighScores:new("mini")
  local isHighScore = tonumber(miniScores:get("mini",5).value) < miniScores:getActualScore("mini")
  local group = scene.view

    local oFix = display.newRect(-200,0,1600,1000)
    group:insert(oFix)
    oFix:setFillColor(0,0,0)
    oFix.alpha = .4
    
    oFix:addEventListener("tap",catchAllTheThings)
    oFix:addEventListener("touch",catchAllTheThings)
    
  
    local background = display.newImage("scenes/resources/gameovermini.png",0,0)
    group:insert(background)
    background:scale(1.6,1.6)
    background.x = 515
    background.y = 370
    
    self.menuBtn = display.newImage("images/buttons/btn_return_menu.png")
    group:insert(self.menuBtn)
    self.menuBtn.x = 490
    self.menuBtn.y = 575
    
    
    local function handlerFunction(event)
      if event.phase == "submitted" then
        
          local enteredText = "Dude"
          pcall(function ()
              enteredText = scene.editField.text
          end )
          if not enteredText then
            enteredText="Player"
          end
          --enteredText now has the value of the text field.
          
          --miniScores:setActualScore("mini",302)
          local score = tonumber(miniScores:getActualScore("mini"))
          miniScores:pushNew("mini",score, enteredText)
          print(enteredText)
          native.setKeyboardFocus(nil)
      end
    end
    
    function self.menuBtn:touch(event)
      --group:removeSelf()
      
      local function hide()
        storyboard.hideOverlay()
        local fakeEv = {}
        fakeEv.phase = "submitted"
        handlerFunction(fakeEv)
        storyboard.gotoScene("main_menu.main")        
        
      end
    
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
      
    end
    
    self.menuBtn:addEventListener( "touch", self.menuBtn )
    
    local scoreText = display.newText(""..miniScores:getActualScore("mini"), 380,405, "Helvetica",45)
    scoreText:setTextColor(245, 245, 250)
    group:insert(scoreText)
    
    
    
    
    
    if isHighScore then
      scene.editField = native.newTextField( 200, 545, 200, 46, handlerFunction )
      --This sets (prefills) the text field value.
      scene.editField.text = "Player"
      scene.editField:setTextColor(0,0,0)
      group:insert(scene.editField)
      local highScoreText = display.newText("New High Score!!", 280,285, "Helvetica",48)
      highScoreText:setTextColor(245, 245, 250)
      group:insert(highScoreText)
    end
end
scene:addEventListener( "createScene" )

function scene:enterScene( event )

end
scene:addEventListener( "enterScene" )

-- the following event is dispatched once the overlay is in place
function scene:overlayBegan( event )
    print( "Showing overlay: " .. event.sceneName )
end
scene:addEventListener( "overlayBegan" )

-- the following event is dispatched once overlay is removed
function scene:overlayEnded( event )
    print( "Overlay removed: " .. event.sceneName )
end
scene:addEventListener( "overlayEnded" )

return scene