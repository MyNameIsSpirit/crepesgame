local storyboard = require "storyboard"
local scene = storyboard.newScene()
storyboard.purgeOnSceneChange = true
function scene:createScene( event )
    -- scene creation code goes here
    local HighScores = require("libs.scores")
  
  
  --print("overlay")
  
    HighScores:display(scene.view)
end
scene:addEventListener( "createScene" )

function scene:enterScene( event )
  -- display scene overlay
  
  --storyboard.hideOverlay()
end
scene:addEventListener( "enterScene" )

-- the following event is dispatched once the overlay is in place
function scene:overlayBegan( event )
    print( "Showing overlay: " .. event.sceneName )
end
scene:addEventListener( "overlayBegan" )

-- the following event is dispatched once overlay is removed
function scene:overlayEnded( event )
    print( "Overlay removed: " .. event.sceneName )
end
scene:addEventListener( "overlayEnded" )

return scene