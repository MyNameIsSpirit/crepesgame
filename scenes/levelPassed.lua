local storyboard = require "storyboard"
local scene = storyboard.newScene()

local function catchAllTheThings() --!
  return true
end

function scene:createScene( event )
    -- scene creation code goes here
  local score = event.params.score
  local bonus = event.params.bonus
  local level = event.params.level 
  local group = scene.view  
  --local HighScores = require("libs.scores")
  
  
  --print("overlay")
    
    --Workaround para bug de isModal
    local oFix = display.newRect(-200,0,1600,1000)
    group:insert(oFix)
    oFix:setFillColor(0,0,0)
    oFix.alpha = .4
    
    oFix:addEventListener("tap",catchAllTheThings)
    oFix:addEventListener("touch",catchAllTheThings)
    
  
    local background = display.newImage("scenes/resources/congrats.png",0,0)
    group:insert(background)
    background:scale(1.6,1.6)
    background.x = 515
    background.y = 370
    
    
    local levelText = display.newText(""..(level - 1),280,395,"Comic Sans MS",35);
    group:insert(levelText)
    levelText:setTextColor( 0, 0, 0, 255 )
    --levelText.text = level
    
    local bonusText = display.newText(""..bonus,280,468,"Comic Sans MS",35);
    group:insert(bonusText)
    bonusText:setTextColor( 0, 0, 0, 255 )
    --bonusText.text = bonus
    
    local scoreText = display.newText(""..(score - bonus),280,535,"Comic Sans MS",35);
    group:insert(scoreText)
    scoreText:setTextColor( 0, 0, 0, 255 )
    --scoreText.text = score - bonus
    
    local continueBtn = display.newImage("images/buttons/btn_continue.png")
    group:insert(continueBtn)
    continueBtn.x = 826
    continueBtn.y = 185
    function continueBtn:touch(event)
      
      local function hide()
        
        storyboard.gotoScene("scenes.levelTrans")
        storyboard.hideOverlay()
        
      end
    
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
    
    end
    
    continueBtn:addEventListener( "touch", continueBtn )
    
    --[[
    self.menuBtn = display.newImage("images/buttons/btn_return_menu.png")
    group:insert(self.menuBtn)
    self.menuBtn.x = 490
    self.menuBtn.y = 575
    
    function self.menuBtn:touch(event)
      --group:removeSelf()
      
      local function hide()
        storyboard.hideOverlay()
        
        storyboard.gotoScene("main_menu.main")        
        
      end
    
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
      
    end
    
    self.menuBtn:addEventListener( "touch", self.menuBtn )
    
    ]]--
end
scene:addEventListener( "createScene" )

function scene:enterScene( event )
  -- display scene overlay
  --local HighScores = require("libs.scores")
  
  
  --print("overlay")
  
  --HighScores:display(self)
  --storyboard.hideOverlay()
end
scene:addEventListener( "enterScene" )

-- the following event is dispatched once the overlay is in place
function scene:overlayBegan( event )
    print( "Showing overlay: " .. event.sceneName )
end
scene:addEventListener( "overlayBegan" )

-- the following event is dispatched once overlay is removed
function scene:overlayEnded( event )
    print( "Overlay removed: " .. event.sceneName )
end
scene:addEventListener( "overlayEnded" )

return scene