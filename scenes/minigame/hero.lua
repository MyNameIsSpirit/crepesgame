local Hero = {}

function Hero:new(stage)
  -- Set Variables
   local _W = display.contentWidth; -- Get the width of the screen
   local _H = display.contentHeight; -- Get the height of the screen
   local motionx = 0; -- Variable used to move character along x axis
   local speed = 20; -- Set Walking Speed
   local character = {}
   
  function self:remove()
    character._functionListeners = nil
    character._tableListeners = nil
    character:removeSelf()
  end
  
  
  function self:display(group)
    self:displaySprites(group)
    
    self:displayButtons(group)
    self:addListeners(group)
    
  end
  function self:addListeners(group)
    function character:collision( event )

      if ( event.phase == "ended" ) then
        if (event.other.type == "good") then
          stage:raiseScore()
        end
        if (event.other.type == "bad") then
          stage:takeDamage()
        end
        if (event.other.type == "bonus") then
          stage:raiseHealth()
        end
      end
    end
     
    character:addEventListener( "collision", character )
  end
  
  function self:displaySprites(group)
    local heroSheetInfo = require("scenes.minigame.minigameCharacter.hero")
    --[[local creamName1 = display.newImage( sheetImageCreamNames ,sheetInfoCreamNames:getFrameIndex("darkchocolate"), -80, 000 ) 
    group:insert(creamName1)]]--
    local heroSheet = graphics.newImageSheet("scenes/minigame/minigameCharacter/hero.png",heroSheetInfo:getSheet())
   
   
    local sequenceData =
    {
        { name="walking", start=1, count=8 },
        {name="standing", start=1, count=1 }
      --[[,
        { name="jumping", start=9, count=13, time=300 }]]--
    }

    character = display.newSprite( heroSheet, sequenceData )
    character.x = 500
    character.y = 550
    character.left = true
    character.running = false
    group:insert(character)
    physics.addBody( character, "dynamic", { friction=0.5, bounce=0 } )
    character.isFixedRotation = true
    character.class = "hero"
    --if not (character.x <= -120 and motionx < 0) and not ( character.x >= 500 and motion > 0) then
    function character:sprite( event )
      if (event.phase == "next") then
        if motionx == 0  then
          character.running = false
          --character:setSequence("standing")
          character:pause()
        end
      end
      if ( event.phase == "loop" ) then
        local thisSprite = event.target  --"event.target" references the sprite
        character.running = false
        --character:setSequence("standing")
        
      end
     
    end
     
    character:addEventListener( "sprite", character) 
    -- Move character
   local function moveguy (event)
    
    if motionx > 0 then
      
    elseif motionx < 0 then
      
    else
      --character:setSequence("standing")
    end
      pcall(function ()
          character.x = character.x + motionx;
          end)
    --end
   end
   Runtime:addEventListener("enterFrame", moveguy)
   
   -- Stop character movement when no arrow is pushed
   local function stop (event)
     if event.phase =="ended" then
      motionx = 0;
      --character.running = false
      --character:setSequence("standing")
      --character:play()
     end
   end
   Runtime:addEventListener("touch", stop )
  end
  
  function self:displayButtons(group)
    local leftBtn = display.newImage("scenes/minigame/resources/btnleft.png")
    group:insert(leftBtn)
    leftBtn:scale(0.5,0.5)
    --leftBtn.alpha = 0.3
    leftBtn.x = 25
    leftBtn.y = 700
    function leftBtn:touch()
      motionx = -speed;
      if not character.left then
        character.left = true
        character:scale(-1, 1); -- Mirror
      end
      if not character.running then
        character.running = true
        character:setSequence( "running" )
        character:play()
      end
    end
    leftBtn:addEventListener("touch",leftBtn)
    -- When right arrow is touched, move character right
    
    
    
    local rightBtn = display.newImage("scenes/minigame/resources/btnright.png")
    group:insert(rightBtn)
    rightBtn:scale(0.5,0.5)
    --rightBtn.alpha = 0.3
    rightBtn.x = 780
    rightBtn.y = 700
    function rightBtn:touch()
      motionx = speed;
      if character.left then
        character.left = false
        character:scale(-1, 1); -- Mirror
      end
      if not character.running then
        character.running = true
        character:setSequence( "running" )
        character:play()
      end
    end
    rightBtn:addEventListener("touch",rightBtn)
    
    
  end
  
  
  
  
  return self
end

return Hero
