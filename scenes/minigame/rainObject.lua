local physics = require "physics"
RainObject ={}
local goodPool = {{"alien",15},{"bag",10},{"dog",10},{"duck",10},{"toy",10},{"watermelon",15},{"world",15},{"000",10},{"001",10},{"002",10},{"004",10},{"005",10},{"006",10}} -- 13
local badPool = {{"shoe",10}, {"poison",10},{"rock",10},{"bomb",10}} -- 4
local bonusPool = {{"heart",10},{"heart2",10},{"heart3",10}} -- 1
local lastBad = 0

function RainObject:new(group,physics,posX,posY,objectType)
  local rainObject = {}
  local selfWrapper = self
  local companion 
  function self:display()
    local pool = goodPool
    local poolSize = 13
    if objectType == "bad" then
      pool = badPool;
      poolSize = 4
    elseif objectType == "bonus" then
      pool = bonusPool
      poolSize = 3
    end
    local randomImage = math.random( 1, poolSize )
    local itemName = pool[randomImage][1]
    local itemRadius = pool[randomImage][2]
    rainObject = display.newImage("scenes/minigame/rain/".. itemName ..".png")
    rainObject.x = posX;
    rainObject.y = posY;
    rainObject.xScale = 0.7
    rainObject.yScale = 0.7
    local rotation
    group:insert(rainObject)
    if randomImage <= 14 and randomImage <= 18 then
      lastBad = rainObject.x
    end
    physics.addBody(rainObject,{density=.4, friction=0.05, bounce=.3, radius=itemRadius,rotation=180})
    rainObject.type = objectType
    rainObject.myName = itemName

    if itemName == "bomb" or itemName == "rock" or itemName == "shoe" then
      local gameMusic1 = audio.loadStream("scenes/minigame/audio/"..itemName..".mp3")
      local backgroundMusicChannel = audio.play(gameMusic1, {channel = 3, loops = 1} )
    end

    if itemName == "bomb"  then
      local explosionFile = require("scenes.minigame.resources.explosion")
      local explosionSheet = graphics.newImageSheet("scenes/minigame/resources/explosion.png",explosionFile:getSheet())
      local sequenceDataHappy = { { name = "normalRun",    start = 1, count = 10, time = 1000, loopCount = 1 } }
      rainObject:removeSelf()
      local explosion = display.newSprite( explosionSheet, sequenceDataHappy )
      rainObject = explosion
      explosion.x = posX;
      explosion.y = posY;
      rainObject.xScale = 0.7
      rainObject.yScale = 0.7
      --companion = explosion
      
      group:insert(explosion)
      physics.addBody(rainObject,{density=.4, friction=0.05, bounce=.3, radius=itemRadius})
      rainObject.type = objectType
      rainObject.myName = itemName
      
       --explosion.alpha = 0;

      function runAnimation()
        explosion.alpha = .8
        explosion.angularVelocity = 0  
        explosion.rotation=0
        explosion.isFixedRotation = true
        pcall(function()
          explosion:play()
        end)
        --timer.performWithDelay(1301, toogleAnimation)
      end

      function toogleAnimation()
        pcall(function()
          explosion:pause()
        end)
        --explosion.alpha = 0
        --explosion:removeSelf()
      end
      explosion.alpha = 1
      --explosion.x, explosion.y = 400, 300
      explosion:scale(1,1)
      --explosion:play()
      timer.performWithDelay(1501, runAnimation)
    end

    if itemName == "rock" or itemName == "poison" or itemName =="shoe" then
      local explosionFile = require("scenes.minigame.resources.danger")
      local explosionSheet = graphics.newImageSheet("scenes/minigame/resources/danger.png",explosionFile:getSheet())
      local sequenceDataHappy = { { name = "normalRun",    start = 1, count = 10, time = 1000, loopCount = 1 } }
      local explosion = display.newSprite( explosionSheet, sequenceDataHappy )
      companion = explosion
      group:insert(companion)
       explosion.alpha = 0;

      function runAnimation()
        explosion.alpha = .4
        explosion:play()
        timer.performWithDelay(1001, toogleAnimation)
      end

      function toogleAnimation()
        --explosion:pause();
       -- explosion.alpha = 0
      end

      explosion.x, explosion.y = lastBad, 700
      explosion:scale(1,1)
      timer.performWithDelay(1001, runAnimation)
    end




    local rotateBallReverse
    local function rotateBall()
      rotation=transition.to( rainObject, { time=3000, rotation=665, onComplete=rotateBallReverse} )
    end

    --rotateBallReverse = function()
    --    transition.to( rainObject, { time=1500, rotation=-365, onComplete=rotateBall} )
    --end
    rotateBall()


    function rainObject:collision( event )
      

      if ( event.phase == "ended" ) then
        
        local function fadeOut(disapearTime) 
          
          selfWrapper:blink(event.target)
          if itemName == "bomb"  then
            transition.cancel(rotation)
          end
          -- cannot remove objects during a collision, so wait a short moment for it to end
          timer.performWithDelay(disapearTime, function()
              -- remove the ball
              timer.performWithDelay(91, function()
                --event.object2.removed= "true"
                pcall(function ()
                          self:removeSelf()
                          companion:removeSelf()
                        end)
              end,1)
          end,1)
        end
        local function disappear()
          timer.performWithDelay(1, function()
                --event.object2.removed= "true"
                if itemName == "bomb"  then
                  transition.cancel(rotation)
                end
                pcall(function ()
                          self:removeSelf()
                          companion:removeSelf()
                        end)
              end,1)
        end
        
        local disapearTime = 90

        if (event.other.class == "hero") then
          disappear()
        elseif event.other.class == "wall" then
          fadeOut(disapearTime)
        else
          fadeOut(disapearTime)
        end
        
      end
    end
     
  rainObject:addEventListener( "collision", rainObject )
  end
 
  function self:getType()
    return objectType
  end
  function self:blink(object)
    if(object.alpha < 1) then
        transition.to( object, {time=90, alpha=1})
    else 
        transition.to( object, {time=90, alpha=0.1})
    end
  end
  
  return self
end

return RainObject

--local explosionSheet = graphics.newImageSheet("scenes/minigame/resources/explosion.png",sheetInfoCharacterHappy:getSheet())
--local explosion = display.newSprite( explosionSheet, sequenceDataHappy )
--explosion.y, animationHappy.x = 200, 425
--explosion:scale(1.3,1.3)
--explosion.alpha = 0