--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:bbef475cc2cd7ff0c89b988df07fed2a:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- radioactivity0000
            x=126,
            y=254,
            width=120,
            height=122,

            sourceX = 3,
            sourceY = 3,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0003
            x=126,
            y=128,
            width=122,
            height=124,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0005
            x=2,
            y=380,
            width=122,
            height=124,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0008
            x=2,
            y=254,
            width=122,
            height=124,

            sourceX = 1,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0011
            x=2,
            y=128,
            width=122,
            height=124,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0013
            x=380,
            y=2,
            width=124,
            height=122,

            sourceX = 1,
            sourceY = 3,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0016
            x=254,
            y=2,
            width=124,
            height=122,

            sourceX = 1,
            sourceY = 3,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0018
            x=128,
            y=2,
            width=124,
            height=122,

            sourceX = 1,
            sourceY = 3,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0023
            x=2,
            y=2,
            width=124,
            height=124,

            sourceX = 1,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- radioactivity0024
            x=126,
            y=128,
            width=122,
            height=124,

            sourceX = 2,
            sourceY = 2,
            sourceWidth = 128,
            sourceHeight = 128
        },
    },
    
    sheetContentWidth = 512,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["radioactivity0000"] = 1,
    ["radioactivity0003"] = 2,
    ["radioactivity0005"] = 3,
    ["radioactivity0008"] = 4,
    ["radioactivity0011"] = 5,
    ["radioactivity0013"] = 6,
    ["radioactivity0016"] = 7,
    ["radioactivity0018"] = 8,
    ["radioactivity0023"] = 9,
    ["radioactivity0024"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
