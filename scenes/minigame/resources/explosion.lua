--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:6a4a481666dd197acdc3152ca2ea7034:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- explosion0002
            x=132,
            y=124,
            width=110,
            height=114,

            sourceX = 10,
            sourceY = 7,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0003
            x=132,
            y=352,
            width=90,
            height=90,

            sourceX = 9,
            sourceY = 31,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0004
            x=132,
            y=352,
            width=90,
            height=90,

            sourceX = 9,
            sourceY = 31,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0005
            x=132,
            y=124,
            width=110,
            height=114,

            sourceX = 10,
            sourceY = 7,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0008
            x=132,
            y=2,
            width=110,
            height=120,

            sourceX = 10,
            sourceY = 7,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0012
            x=132,
            y=124,
            width=110,
            height=114,

            sourceX = 10,
            sourceY = 7,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0014
            x=2,
            y=126,
            width=128,
            height=122,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0015
            x=2,
            y=2,
            width=128,
            height=122,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0018
            x=2,
            y=250,
            width=128,
            height=112,

            sourceX = 0,
            sourceY = 16,
            sourceWidth = 128,
            sourceHeight = 128
        },
        {
            -- explosion0024
            x=132,
            y=240,
            width=102,
            height=110,

            sourceX = 13,
            sourceY = 17,
            sourceWidth = 128,
            sourceHeight = 128
        },
    },
    
    sheetContentWidth = 256,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["explosion0002"] = 1,
    ["explosion0003"] = 2,
    ["explosion0004"] = 3,
    ["explosion0005"] = 4,
    ["explosion0008"] = 5,
    ["explosion0012"] = 6,
    ["explosion0014"] = 7,
    ["explosion0015"] = 8,
    ["explosion0018"] = 9,
    ["explosion0024"] = 10,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
