--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:4c1a248e1af4375d850be7f854d6933b:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- left0000
            x=2,
            y=2,
            width=124,
            height=125,

        },
        {
            -- left0001
            x=2,
            y=2,
            width=124,
            height=125,

        },
        {
            -- left0002
            x=126,
            y=250,
            width=122,
            height=123,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0003
            x=126,
            y=250,
            width=122,
            height=123,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0004
            x=124,
            y=375,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0005
            x=2,
            y=381,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0006
            x=2,
            y=381,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0007
            x=128,
            y=2,
            width=124,
            height=119,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0008
            x=128,
            y=2,
            width=124,
            height=119,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0009
            x=2,
            y=129,
            width=122,
            height=123,

            sourceX = 1,
            sourceY = 2,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0010
            x=2,
            y=254,
            width=120,
            height=125,

            sourceX = 4,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0011
            x=2,
            y=254,
            width=120,
            height=125,

            sourceX = 4,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- left0012
            x=128,
            y=123,
            width=122,
            height=125,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
    },
    
    sheetContentWidth = 256,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["left0000"] = 1,
    ["left0001"] = 2,
    ["left0002"] = 3,
    ["left0003"] = 4,
    ["left0004"] = 5,
    ["left0005"] = 6,
    ["left0006"] = 7,
    ["left0007"] = 8,
    ["left0008"] = 9,
    ["left0009"] = 10,
    ["left0010"] = 11,
    ["left0011"] = 12,
    ["left0012"] = 13,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
