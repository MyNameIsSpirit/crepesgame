--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:ab1f8f5fad72a8a78a9585fc3bbb7f4d:1/1$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- Untitled-10000
            x=2,
            y=2,
            width=124,
            height=125,

        },
        {
            -- Untitled-10001
            x=2,
            y=2,
            width=124,
            height=125,

        },
        {
            -- Untitled-10002
            x=126,
            y=250,
            width=122,
            height=123,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10003
            x=126,
            y=250,
            width=122,
            height=123,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10004
            x=124,
            y=375,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10005
            x=2,
            y=381,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10006
            x=2,
            y=381,
            width=120,
            height=125,

            sourceX = 2,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10007
            x=128,
            y=2,
            width=124,
            height=119,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10008
            x=128,
            y=2,
            width=124,
            height=119,

            sourceX = 0,
            sourceY = 6,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10009
            x=2,
            y=129,
            width=122,
            height=123,

            sourceX = 1,
            sourceY = 2,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10010
            x=2,
            y=254,
            width=120,
            height=125,

            sourceX = 4,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10011
            x=2,
            y=254,
            width=120,
            height=125,

            sourceX = 4,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
        {
            -- Untitled-10012
            x=128,
            y=123,
            width=122,
            height=125,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 124,
            sourceHeight = 125
        },
    },
    
    sheetContentWidth = 256,
    sheetContentHeight = 512
}

SheetInfo.frameIndex =
{

    ["Untitled-10000"] = 1,
    ["Untitled-10001"] = 2,
    ["Untitled-10002"] = 3,
    ["Untitled-10003"] = 4,
    ["Untitled-10004"] = 5,
    ["Untitled-10005"] = 6,
    ["Untitled-10006"] = 7,
    ["Untitled-10007"] = 8,
    ["Untitled-10008"] = 9,
    ["Untitled-10009"] = 10,
    ["Untitled-10010"] = 11,
    ["Untitled-10011"] = 12,
    ["Untitled-10012"] = 13,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
