--  local physics = require "physics"
 local Object = require "scenes.minigame.rainObject" 
 local Rain = {}
 
function listener2()
  print("FiN")
end

 function Rain:new(stage)
  --local cloudType = math.random(1,3)
  --cloud = display.newImage("scenes/minigame/resources/cloud".. cloudType ..".png")
  --w = 50
  --h = 50
  --cloud.x = 50
  --cloud.y = 50
  --cloud.alpha = 0.6
  --transition.to( square, { time=500, delay=2500, alpha=0.6, x=(w-50), y=(h-50) ,onComplete=listener2 } )

  self.scene = nil
  self.group = nil
  self.phisics = nil
  self.dropFreq = 2500
  self.dropTimer = nil
  self.badProbability = 10
  self.bonusProbability = 90
  
  function self:startRain()
    local function listener()
      Rain:dropItem()
    end
    self.dropTimer = timer.performWithDelay(self.dropFreq,listener,0)
  end
  function self:stopRain()
    timer.cancel(self.dropTimer)
  end
  
  function self:randomPosition()
    return math.random(-100,750)
  end
  function self:randomHeight()
    return -150
  end
  function self:randomAccel()
    return 9.8
  end
  function self:dropItem()
    local itemType = "good"
    local randomType = math.random(0,100)
    if randomType < self.badProbability then
      itemType = "bad"
    elseif randomType > self.bonusProbability then
      itemType = "bonus"
    end
    local rainObject = Object:new(self.group,self.physics,self:randomPosition(),self:randomHeight(),itemType)
    rainObject:display()
  end
  function self:raiseLevel()
    self:stopRain()
    self.dropFreq = self.dropFreq * 0.85
    if self.badProbability < 70 then
      self.badProbability = self.badProbability * 1.25
    end
    self.bonusProbability = 100 - (100 - self.bonusProbability)*.85
    self:startRain()
  end
  function self:display(scene,group,physics)
    self.scene = scene
    self.group = group
    self.physics = physics
  end
  
  return self
 end


return Rain