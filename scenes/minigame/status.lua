local storyboard = require "storyboard" 
 local StatusBar = {}
 local HighScores = require "libs.scores"
 
 local function catchAllTheThings()
  return true
 end
 function StatusBar:new(stage)
    self.score = HighScores:new("mini")
    function self:resetScore()
      self.score:setActualScore("mini",0)      
    end
    function self:getScore()
      return self.score:getActualScore("mini")
    end
    function self:raiseScore()
      self.score:setActualScore("mini",self.score:getActualScore("mini")+50)   
      local gameMusic1 = audio.loadStream("scenes/minigame/audio/item.mp3")  
      local backgroundMusicChannel = audio.play(gameMusic1, {channel = 2, loops = 1 } )   
    end
    function self:takeDamage()
      if self.health ==3 then
        self.health = 2
        self.heart3.alpha = 0
      elseif self.health == 2 then
        self.health = 1
        self.heart2.alpha = 0        
      elseif self.health == 1 then
        self.heart1.alpha = 0
        self.health = 0
      else
        stage:finishGame()       
      end
    end
    function self:raiseHealth()
      if self.health ==2 then
        self.health = 3
        self.heart3.alpha = 1
      elseif self.health == 1 then
        self.health = 2
        self.heart2.alpha = 1        
      elseif self.health == 0 then
        self.heart1.alpha = 1
        self.health = 1       
      end
    end
   
    function self:display(group)
      local kindlePadding = 0
      if (system.getInfo("model") == "KFJWI" or system.getInfo("model") == "KFTT") then
        kindlePadding = -40
      end
      local oFix = display.newRect(900 + kindlePadding,0,400,1000)
      group:insert(oFix)
      oFix:setFillColor(0,0,0)
      oFix.alpha = .9
    
      oFix:addEventListener("tap",catchAllTheThings)
      oFix:addEventListener("touch",catchAllTheThings)
    
      
      local timmer = display.newImage("images/commonresources/timmer.png", 860 + kindlePadding, 620)
      group:insert(timmer)
      timmer:scale(0.5,0.7)
      local coin = display.newImage("images/commonresources/coin.png", 910 + kindlePadding, 635)
      group:insert(coin)
      coin:scale(0.75,0.75)
      
      
  
      self.score.levelText = display.newText(""..0, 1071 + kindlePadding,655, "Comic Sans MS",50)
      self.score.levelText:setTextColor(0, 0, 0)
      group:insert(self.score.levelText)
      --hay un problema con el despliegue del texto al inicializar se desplaza
      --asi que se inicia como 0 y se obliga a actualizarse
      
      self.score:setActualScore("mini",self.score:getActualScore("mini"))
      
      
      --Set text
      local lifeText = display.newText("Life", 1000 + kindlePadding,5, "Helvetica",40)
      lifeText:setTextColor(250, 250, 250)
      group:insert(lifeText)
      
      --Set hearts
      self.heart1 = display.newImage("scenes/minigame/resources/heart.png", 950 + kindlePadding, 50)
      group:insert(self.heart1)
      self.heart1:scale(.75,.75)
      self.heart2 = display.newImage("scenes/minigame/resources/heart.png", 950 + kindlePadding, 180)
      group:insert(self.heart2)
      self.heart2:scale(.75,.75)
      self.heart3 = display.newImage("scenes/minigame/resources/heart.png", 950 + kindlePadding, 310)
      group:insert(self.heart3)
      self.heart3:scale(.75,.75)
      
      self.health = 3
      
      --Set return to menu
      self.menuBtn = display.newImage("images/buttons/btn_return_menu.png")
      self.menuBtn:scale(.7,.7)
      group:insert(self.menuBtn)
      self.menuBtn.x = 970 + kindlePadding
      self.menuBtn.y = 550
      
      --Set return to menu text
      local menuText = display.newText("Pause", 1040 + kindlePadding,520, "Helvetica",45)
      menuText:setTextColor(245, 245, 250)
      group:insert(menuText)
    
       
      function self.menuBtn:touch(event)
        local function hide()
          stage:pause()
          --storyboard.gotoScene("main_menu.main")        
          
        end
        if event.phase == "ended" then
          timer.performWithDelay(100,hide)
        end
      end
      self.menuBtn:addEventListener( "touch", self.menuBtn )
    end
    return self
end

return StatusBar

