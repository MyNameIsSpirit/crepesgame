 local storyboard = require "storyboard"
 local StatusBar = require "scenes.minigame.status" 
 local ObjectsRain = require "scenes.minigame.rain" 
 local Hero = require "scenes.minigame.hero" 
 local HighScores = require "libs.scores"
 local physics = require "physics"
 local Stage = {}
 local gameMusic1 = audio.loadStream("scenes/minigame/audio/minigame.mp3")


 function Stage:new()
   local backgroundMusicChannel = audio.play(gameMusic1, {channel = 1, loops = -1, fadein = 5000 } )
    self.statusBar = StatusBar:new(self)
    self.floor = nil
    self.rain = ObjectsRain:new(self)
    self.hero = Hero:new(self)
    self.gravity = 3.81
    --self.score = HighScores:new("mini")
    
    function self:resetScore()
      self.statusBar:resetScore()
    end
    function self:startPhysics(group)
      --TODO:
      physics.start()
      physics.setGravity(0, self.gravity)  -- 9.81 m/s*s in the positive x direction  
      physics.setScale(80)  -- 80 pixels per meter  
      physics.setDrawMode("normal")  
      local staticMaterial = {density=2, friction=.3, bounce=.4} 
      physics.addBody(self.floor, "static", staticMaterial) 
      physics.addBody(self.wallRight, "static", staticMaterial) 
      physics.addBody(self.wallLeft, "static", staticMaterial) 
    end
    function self:getScore()
      return self.statusBar:getScore()
    end
    function self:startRain()
      self.rain:startRain()
    end
    function self:stopRain()
      self.rain:stopRain()
    end
    function self:finishGame()
      physics.pause()
      self:stopRain()
      local options =
      {
          effect = "fade",
          time = 400
      }
      pcall(function ()
        gameMusic1 = audio.stop(gameMusic1)
      end)
      storyboard.showOverlay("scenes.miniGameOver", options)   
    end
    function self:pause()
      self.gameIsPaused = true
      physics.pause()
      self:stopRain()
      local function continue()
        physics.start()
        self:startRain()
      end
      local function menu()
        self:finishGame("loose")
      end
      local options =
      {
          effect = "fade",
          time = 400,
          isModal = true,
          params = {parentScene = self.scene,continueListener =continue,beforeMenuListener=menu}
          
      }
      storyboard.showOverlay( "scenes.gamePaused", options )
    end
    function self:takeDamage()
      self.statusBar:takeDamage()
    end
    function self:raiseHealth()
      self.statusBar:raiseHealth()
    end
    
    function self:raiseScore()
      self.statusBar:raiseScore()
      if self:getScore() % 200 == 0  then
        self.rain:raiseLevel()
        self.gravity = self.gravity*1.05
        physics.setGravity(0, 3.81)
      end
    end
    function self:display(scene)
      local group = scene.view
      self.scene = scene
      local background = display.newImage("scenes/minigame/resources/background.png",-120,-220)
      --background:scale(1,1)
      
      group:insert(background)
      
      self.floor = display.newRect(-175, 688, 1075, 1) 
      self.floor.class = "wall"
      self.floor.myName = "wall"
      group:insert(self.floor)
      
      
      self.wallLeft = display.newRect(-120, 10, 1, 1200) 
      self.wallLeft.alpha = 0
      self.wallLeft.class = "wall"
      self.wallLeft.myName = "wallLeft"
      group:insert(self.wallLeft)
      self.wallRight = display.newRect(860, 0, 1, 1200) 
      self.wallRight.class = "wall"
      self.wallRight.myName = "wallRight"
      group:insert(self.wallRight)
      
      
      self.statusBar:display(group)
      self.rain:display(scene,group,physics)
      self.rain:startRain()
      
      self:startPhysics(group)
      self.hero:display(group)
      --self.rain:display(stageGroup)
      --self:finishGame()
      
    end
    return self
 end
 
 
return Stage