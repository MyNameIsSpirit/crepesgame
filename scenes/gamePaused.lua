local storyboard = require "storyboard"
local scene = storyboard.newScene()
storyboard.purgeOnSceneChange = true
local function catchAllTheThings()
  return true
end

function scene:createScene( event )
     -- display scene overlay
  local parentScene = event.params.parentScene
  local continueListener = event.params.continueListener
  local beforeMenuListener = event.params.beforeMenuListener
  local group = scene.view  
  local HighScores = require("libs.scores")
  
  
  print("overlay")
    
    --Workaround para bug de isModal
    local oFix = display.newRect(-200,0,1600,1000)
    group:insert(oFix)
    oFix:setFillColor(0,0,0)
    oFix.alpha = .4
    
    oFix:addEventListener("tap",catchAllTheThings)
    oFix:addEventListener("touch",catchAllTheThings)
    
  
    local background = display.newImage("scenes/resources/pause.png",0,0)
    group:insert(background)
    --background:scale(1.25,1.2)
    background.x = 515
    background.y = 370
    
    local continueBtn = display.newImage("images/buttons/btn_continue.png")
    group:insert(continueBtn)
    continueBtn.x = 526
    continueBtn.y = 405
    function continueBtn:touch(event)
      
      local function hide()
        print("hidding?")
        parentScene.gameIsPaused=false
        if continueListener then
          continueListener()
        end
        storyboard.hideOverlay()
        
      end
    
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
    
    end
    
    continueBtn:addEventListener( "touch", continueBtn )
    
    
    self.menuBtn = display.newImage("images/buttons/btn_return_menu.png")
    group:insert(self.menuBtn)
    self.menuBtn.x = 490
    self.menuBtn.y = 575
    
    function self.menuBtn:touch(event)
      --group:removeSelf()
      
      local function hide()
        storyboard.hideOverlay()
        if beforeMenuListener then
          beforeMenuListener()
        end
        storyboard.gotoScene("main_menu.main")        
        
      end
    
      if event.phase == "ended" then
        timer.performWithDelay(100,hide)
      end
      
    end
    
    self.menuBtn:addEventListener( "touch", self.menuBtn )
    
    --group:insert(menuBtn)
    --HighScores:display(self)
  --storyboard.hideOverlay()
end
scene:addEventListener( "createScene" )

function scene:enterScene( event )
  
end
scene:addEventListener( "enterScene" )

-- the following event is dispatched once the overlay is in place
function scene:overlayBegan( event )
    print( "Showing overlay: " .. event.sceneName )
end
scene:addEventListener( "overlayBegan" )

-- the following event is dispatched once overlay is removed
function scene:overlayEnded( event )
    
    print( "Overlay removed: " .. event.sceneName )
end
scene:addEventListener( "overlayEnded" )

return scene 
